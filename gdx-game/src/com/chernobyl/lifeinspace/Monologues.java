package com.chernobyl.lifeinspace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.I18NBundle;

public class Monologues
{	

	private static I18NBundle bundle;
	static {
		loadBundle("messages/Monologues");
	}
	
	public static void loadBundle(String bundleName)
	{
		bundle = I18NBundle.createBundle(Gdx.files.internal(bundleName));
	}

	public static String GetTextFromId(int textId)
	{
		return bundle.get(textId + "");
	}
}
