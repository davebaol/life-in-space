package com.chernobyl.lifeinspace;

import java.util.ArrayList;
import java.util.TreeMap;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.chernobyl.common.render.Billboard;
import com.chernobyl.common.render.RenderComposition;
import com.chernobyl.common.render.RenderData;
import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.JBTHelpers.AiType;
import com.chernobyl.common.utils.JBTHelpers.TriggerType;
import com.chernobyl.common.utils.IntrusiveLinkedList;

public class Planet
{	
	static final float backgroundSpeed =
		2.f * (float)Math.PI / 180.f; // full turn in 3mn

	Billboard surface;
	Billboard background;

	String switchTex;
	
	Vector2 center;
	float radius;
	
	float angle;
	float angleSpeed;
	
	float backgroundAngle;
	
	ArrayList<Entity> entities;

	TreeMap<Integer, ArrayList<Entity>> renderEntities;
	
	boolean updateLock;
	ArrayList<Entity> requestRemoval;
	ArrayList<Entity> requestAddition;
	
	IntrusiveLinkedList<Entity> sorted;
	
	Planet(float radius, String planetTexture, String backgroundTexture, String switchTexture)
	{
		this.radius = radius;
		
		this.angle = 0.f;
		center = new Vector2();
		
		entities = new ArrayList<Entity>();
		requestAddition = new ArrayList<Entity>();
		requestRemoval = new ArrayList<Entity>();
		
		sorted = new IntrusiveLinkedList<Entity>();

		renderEntities = new TreeMap<Integer, ArrayList<Entity>>();
		
		float size = radius * 2.f;
		surface = new Billboard(planetTexture,
			size, size, false);

		float backgroundSize = (radius < 1.f) ?
			(float)Math.sqrt(radius) * 2.f * 1.8f : size * 1.8f;
		background = new Billboard(backgroundTexture,
				backgroundSize, backgroundSize, false);
				
		backgroundAngle = 0.f;

		switchTex = switchTexture;
	}

	public void AddEntity(Entity entity, float angle)
	{
		entity.angle = angle;
		entity.SetDist(radius);
		
		if (updateLock)
			requestAddition.add(entity);
		else
			entities.add(entity);	
		
		sorted.InsertTail(entity);
		entity.RefreshSort(false);
		entity.RefreshSort(true);

		int renderOrder = entity.renderOrder;
		ArrayList<Entity> renderList = renderEntities.get(renderOrder);
		if (null == renderList)
		{
			renderList = new ArrayList<Entity>();
			renderEntities.put(renderOrder, renderList);
		}

		renderList.add(entity);
	}
	
	public void RemoveEntity(Entity entity)
	{
		if (updateLock)
			requestRemoval.add(entity);
		else
			entities.remove(entity);

		sorted.Remove(entity);

		int renderOrder = entity.renderOrder;
		ArrayList<Entity> renderList = renderEntities.get(renderOrder);
		renderList.remove(entity);
		if (renderList.isEmpty())
		{
			renderEntities.remove(renderOrder);
		}
	}
	
	static public class Vegetation extends Entity
	{
		ArrayList<Seed> seeds;
		
		Vegetation()
		{
			super(new Billboard(
					"planet/plant.png"
					, 0.1f
					, 0.1f));

			EnableAI(TriggerType.VEGETATION_CYCLE);
			
			seeds = new ArrayList<Seed>();
		}
		
		@Override
		public void EnterPlanet(Planet planet, float angle)
		{
			super.EnterPlanet(planet, angle);
			OffsetDist(-0.01f);
		}
		
		@Override
		public String GetInteractionType()
		{
			return "Vegetation";
		}
		
		@Override
		public void Use(Character character)
		{
			if (seeds.isEmpty())
			{
				float xoffsetToAngle = MathUtils.degRad / curPlanet.radius;
				
				Seed seed1 = new Seed();
				float angleSeed1 = (angle + 0.75f * xoffsetToAngle);
				seed1.EnterPlanet(curPlanet, angleSeed1);
				seed1.OffsetDist(0.04f);
				
				seeds.add(seed1);

				Seed seed2 = new Seed();
				float angleSeed2 = (angle + 1.5f * xoffsetToAngle);
				seed2.EnterPlanet(curPlanet, angleSeed2);
				seed2.OffsetDist(0.02f);
				
				seeds.add(seed2);
			}
			else
			{
				for (Seed seed : seeds)
				{
					seed.FallToGround(GetDist());
				}
				seeds.clear();
			}
		}
		
		@Override
		public void Hide(boolean hide)
		{
			for (Seed seed : seeds)
			{
				seed.Hide(hide);
			}
		}
		
		@Override
		public void Release()
		{
			for (Seed seed : seeds)
			{
				curPlanet.RemoveEntity(seed);
				seed.Release();
			}
			seeds.clear();
			
			super.Release();
		}
	}
	
	static public class EatenVegetation extends Entity
	{
		EatenVegetation()
		{
			super(new Billboard(
				"planet/plant_eaten.png"
				, 0.1f
				, 0.1f));
		}
		
		@Override
		public void EnterPlanet(Planet planet, float angle)
		{
			super.EnterPlanet(planet, angle);
			OffsetDist(-0.01f);
		}
		
		@Override
		public String GetInteractionType()
		{
			return "EatenVegetation";
		}
	}

	static public class Seed extends Entity
	{
		Seed()
		{
			super(new Billboard(
					"planet/seed.png"
					, 0.03f
					, 0.03f));

			Billboard visual = (Billboard)renderData;
			visual.SetRot(110.f);

			haloEnabled = false;
		}

		@Override
		public String GetInteractionType()
		{
			return "Seed";
		}
		
		public void FallToGround(float groundDist)
		{
			jumpHeight = GetDist() - groundDist;
			SetDist(groundDist);
			
			EnableAI(TriggerType.SEED_CYCLE);
		}
		
		@Override
		public void Use(Character character)
		{
			PlantNewTree();
			
			curPlanet.RemoveEntity(this);
			Release();
		}
		
		private void PlantNewTree()
		{
			Entity prevEntity = GetPrevEntity();
			while (prevEntity != null && prevEntity != this &&
				!prevEntity.GetInteractionType().equals("Vegetation"))
			{
				prevEntity = prevEntity.GetPrevEntity();
			}
			
			if (Math.abs(curPlanet.Distance(this, prevEntity)) < 0.2f)
				return;

			Entity nextEntity = GetNextEntity();
			while (nextEntity != null && nextEntity != this &&
				!nextEntity.GetInteractionType().equals("Vegetation"))
			{
				nextEntity = nextEntity.GetNextEntity();
			}
			
			if (Math.abs(curPlanet.Distance(this, nextEntity)) < 0.2f)
				return;

			Vegetation veget = new Vegetation();
			veget.EnterPlanet(curPlanet, angle);			
		}
	}
	
	static public class Rock extends Entity
	{
		RenderComposition comp;
		int count = 3;
		
		Rock()
		{
			super(new RenderComposition());
			
			comp = (RenderComposition)renderData;

			final String parts[] =
				{"middle", "left", "right"};
			
			for (String name : parts)
			{
				String asset = "planet/rock_"
					+ name + ".png";
				
				comp.AddPart(name,
					new Billboard(
						asset, 0.2f, 0.1f));
			}
		}

		@Override
		public void Use(Character character)
		{
			float dist =
				curPlanet.Distance(this, character);
				
			if (dist > 0.f &&
				!comp.IsPartHidden("right"))
			{
				comp.HidePart("right", true);
				count--;
			}
			else if (dist < 0.f &&
				!comp.IsPartHidden("left"))
			{
				comp.HidePart("left", true);
				count--;
			}
			else if (!comp.IsPartHidden("middle"))
			{
				comp.HidePart("middle", true);
				count--;
			}
			
			if (count == 0)
			{
				// force remove
				Release();
			}
		}
		
		@Override
		public void EnterPlanet(Planet planet, float angle)
		{
			super.EnterPlanet(planet, angle);
			OffsetDist(-0.02f);
		}
		
		@Override
		public String GetInteractionType()
		{
			return "Rock";
		}
		
		@Override
		public boolean IsCollider()
		{
			return true;
		}
	}

	static public class Flame extends Entity
	{
		RenderComposition comp;

		static final String parts[] =
				{"flame1", "flame2", "flame3"};

		float animTimer;
		int animStep = 0;

		static final float animSwitchDelay = 0.4f;

		Flame()
		{
			super(new RenderComposition());

			comp = (RenderComposition)renderData;

			for (String name : parts)
			{
				String assetFile = "rocket/"
						+ name + ".png";

				Billboard partData = new Billboard(
						assetFile, 0.2f, 0.1f);

				comp.AddPart(name, partData, true);
			}

			comp.HidePart(parts[animStep], false);
			animTimer = animSwitchDelay;
		}

		@Override
		public void Update(float deltatime)
		{
			animTimer -= deltatime;
			if (animTimer <= 0.f)
			{
				comp.HidePart(parts[animStep], true);
				++animStep;
				if (animStep == parts.length)
				{
					animStep = 0;
				}
				comp.HidePart(parts[animStep], false);

				animTimer = animSwitchDelay;
			}

			super.Update(deltatime);
		}

		@Override
		public String GetInteractionType()
		{
			return "Flame";
		}

		@Override
		public boolean IsCollider()
		{
			return false;
		}
	}
	
	static public class PlanetSwitch extends Entity
	{
		Planet planet;
		
		PlanetSwitch(Planet planet, TriggerType triggerType)
		{
			super(new Billboard(
				planet.switchTex
				, 0.10f
				, 0.20f));
			
			this.planet = planet;
			
			Hide(true);
			EnableAI(triggerType);
		}
		
		@Override
		public String GetInteractionType()
		{
			return "PlanetSwitch";
		}
		
		@Override 
		public void Use(Character character)
		{			
			character.EnterPlanet(planet, 0.f);
		}
	}
	
	public void AddVegetation(float angleDeg)
	{
		AddVegetation(angleDeg, false);
	}
	
	public void AddVegetation(float angleDeg, boolean eaten)
	{
		Entity veget = eaten ? 
			new EatenVegetation() : new Vegetation();
		
		float angleRad = angleDeg * MathUtils.degRad;
		veget.EnterPlanet(this, angleRad);
	}
	
	public void AddVegetationTuto(float angleDeg)
	{
		Entity veget = new Vegetation()
		{
			@Override
			public String GetInteractionType()
			{
				return "VegetationTuto";
			}
		};
			
		float angleRad = angleDeg * MathUtils.degRad;
		veget.EnterPlanet(this, angleRad);
		
		veget.EnableAI(TriggerType.DISCOVER_VEGETATION_TUTO);
	}

	public void AddBush(float angleDeg)
	{
		AddBush(angleDeg, 0.f);
	}

	public void AddBush(float angleDeg, float heightOffset)
	{
		boolean collides = (heightOffset == 0.f);

		float angleRad = angleDeg * MathUtils.degRad;

		Billboard bushBack = new Billboard(
				"planet/bush_back.png"
				, 0.5f
				, 0.25f);

		Entity bushBackEntity;
		if (collides)
		{
			bushBackEntity = new Entity(bushBack)
			{
				@Override
				public String GetInteractionType()
				{
					return "Bush";
				}

				@Override
				public boolean IsCollider()
				{
					return true;
				}
			};

			bushBackEntity.collWidthRatio = 0.5f;

			bushBackEntity.collPlayerOnly = true;

			bushBackEntity.renderOrder = -50;
		}
		else
		{
			bushBackEntity = new Entity(bushBack)
			{
				@Override
				public String GetInteractionType()
				{
					return "Bush";
				}

				@Override
				public boolean IsCollider()
				{
					return false;
				}
			};

			bushBackEntity.renderOrder = 60;
		}

		bushBackEntity.EnterPlanet(this, angleRad);
		bushBackEntity.OffsetDist(heightOffset - 0.06f);

		Billboard bushFront = new Billboard(
				"planet/bush_front.png"
				, 0.5f
				, 0.25f);

		Entity bushFrontEntity;
		if (collides)
		{
			bushFrontEntity = new Entity(bushFront)
			{
				@Override
				public String GetInteractionType()
				{
					return "Bush";
				}

				@Override
				public boolean IsCollider()
				{
					return false;
				}
			};

			bushFrontEntity.renderOrder = 50;
		}
		else
		{
			bushFrontEntity = new Entity(bushFront)
			{
				@Override
				public String GetInteractionType()
				{
					return "Bush";
				}

				@Override
				public boolean IsCollider()
				{
					return false;
				}
			};

			bushFrontEntity.renderOrder = 60;
		}

		bushFrontEntity.EnterPlanet(this, angleRad);
		bushFrontEntity.OffsetDist(heightOffset - 0.06f);
	}
	
	public void AddRock(float angleDeg)
	{				
		Entity rock = new Rock();
		
		float angleRad = angleDeg * MathUtils.degRad;
		rock.EnterPlanet(this, angleRad);
	}
	
	public void AddRockTuto(float angleDeg)
	{				
		Entity rock = new Rock()
		{
			@Override
			public String GetInteractionType()
			{
				return "RockTuto";
			}
		};
		
		float angleRad = angleDeg * MathUtils.degRad;
		rock.EnterPlanet(this, angleRad);
		
		rock.EnableAI(TriggerType.DISCOVER_ROCKS);
	}

	void AddAnimal(float angleDeg)
	{		
		Character animal = new Character.Animal()
		{
			int seedDropCounter = 1;

			static final float dropDelay = 1.f;
			static final float dropRepeat = 10.f;
			float dropTimer = dropDelay;

			@Override
			public void Update(float time)
			{
				super.Update(time);

				if (IsHolding())
				{
					if (seedDropCounter > 0)
					{
						if (dropTimer > 0.f)
						{
							dropTimer -= time;
						}

						if (dropTimer <= 0.f)
						{
							Seed seed = new Seed();
							seed.EnterPlanet(curPlanet, angle);

							seed.DisableAI(); // force reset
							seed.FallToGround(GetDist());

							//--seedDropCounter;
							//dropTimer = dropDelay;

							// drop seeds regularly for now
							dropTimer = dropRepeat;
						}
					}
				}
				else
				{
					dropTimer = dropDelay;
					seedDropCounter = 1;
				}
			}
		};

		animal.renderOrder = 5;

		float angleRad = angleDeg * MathUtils.degRad;
		animal.EnterPlanet(this, angleRad);
		
		animal.EnableAI(AiType.ANIMAL);
	}

	public void AddShelter(float angleDeg)
	{
		float angleRad = angleDeg * MathUtils.degRad;

		Billboard shelter = new Billboard(
				"planet/shelter.png"
				, 0.2f
				, 0.1f);

		Entity shelterEntity = new Entity(shelter)
		{
			@Override
			public String GetInteractionType()
			{
				return "Shelter";
			}
			
			@Override
			public void EnterPlanet(Planet planet, float angle)
			{
				super.EnterPlanet(planet, angle);
				OffsetDist(-0.02f);
			}

			@Override
			public void Use(Character character)
			{
				if (character.IsHolding())
				{
					Entity droppedEntity = character.holdedEntity;
					character.StopHolding();
					droppedEntity.Release();
				}
			}
		};

		shelterEntity.renderOrder = 6;

		shelterEntity.haloEnabled = false;

		shelterEntity.EnterPlanet(this, angleRad);
	}

	void AddHuman(float angleDeg)
	{		
		Character human = new Character.Human();
		human.renderOrder = 10;

		float angleRad = angleDeg * MathUtils.degRad;
		human.EnterPlanet(this, angleRad);
		
		human.EnableAI(AiType.TEST);

		human.OffsetDist(0.006f);

		human.haloEnabled = false;
	}

	public void AddRocket(float angleDeg, float height)
	{
		Billboard mainPart = new Billboard(
				"rocket/rocket.png"
				, 0.5f
				, 0.25f);

		Entity mainPartEntity = new Entity(mainPart)
		{
			@Override
			public String GetInteractionType()
			{
				return "Rocket";
			}

			@Override
			public boolean IsCollider()
			{
				return false;
			}
		};

		float angleRad = angleDeg * MathUtils.degRad;
		float mainFlameAngleRad = (angleDeg - 7.5f) * MathUtils.degRad;
		float secFlameAngleRad = (angleDeg - 6.8f) * MathUtils.degRad;

		float angleSpeed = 5.f * MathUtils.degRad;

		mainPartEntity.EnterPlanet(this, angleRad);
		mainPartEntity.OffsetDist(height);
		mainPartEntity.angleSpeed = angleSpeed;

		Entity flameB = new Flame();
		flameB.EnterPlanet(this, secFlameAngleRad);
		flameB.OffsetDist(height + 0.14f);
		flameB.angleSpeed = angleSpeed;

		Entity flameC = new Flame();
		flameC.EnterPlanet(this, secFlameAngleRad);
		flameC.OffsetDist(height - 0.02f);
		flameC.angleSpeed = angleSpeed;

		Entity flameA = new Flame();
		flameA.EnterPlanet(this, mainFlameAngleRad);
		flameA.OffsetDist(height + 0.05f);
		flameA.scale *= 1.2f;
		flameA.angleSpeed = angleSpeed;
	}

	public void AddModule(float angleDeg, boolean open, boolean zoomed)
	{
		float scale = zoomed ? 0.5f : 0.08f;

		Billboard module = new Billboard(
				open ? "rocket/module_open.png" : "rocket/module.png"
				, scale
				, scale * 0.5f
				, false);

		module.SetRot(90.f);

		Entity moduleEntity;

		if (open)
		{
			moduleEntity = new Entity(module)
			{
				@Override
				public String GetInteractionType()
				{
					return "Module";
				}

				@Override
				public boolean IsCollider()
				{
					return false;
				}
			};
		}
		else
		{
			RenderComposition compModule = new RenderComposition();

			float flameScale = 0.15f;

			Billboard flameA = new Billboard(
					"rocket/flame1.png"
					, scale * flameScale
					, scale * flameScale * 0.5f);

			flameA.SetPivot(0.75f * flameA.GetBounds().getWidth(), 0.5f * flameA.GetBounds().getHeight());
			flameA.SetPosRot(-0.12f * scale, -0.02f * scale, 90.f);

			compModule.AddPart("flameA", flameA);

			Billboard flameB = new Billboard(
					"rocket/flame1.png"
					, scale * flameScale
					, scale * flameScale * 0.5f);

			flameB.SetPivot(0.75f * flameB.GetBounds().getWidth(), 0.5f * flameB.GetBounds().getHeight());
			flameB.SetPosRot(0.01f * scale, 0.02f * scale, 125.f);

			compModule.AddPart("flameB", flameB);

			Billboard flameC = new Billboard(
					"rocket/flame1.png"
					, scale * flameScale
					, scale * flameScale * 0.5f);

			flameC.SetPivot(0.75f * flameC.GetBounds().getWidth(), 0.5f * flameC.GetBounds().getHeight());
			flameC.SetPosRot(-0.24f * scale, -0.01f * scale, 55.f);

			compModule.AddPart("flameC", flameC);

			compModule.AddPart("main", module);

			moduleEntity = new Entity(compModule)
			{
				boolean started = false;
				boolean finished = false;

				float landingHeight;

				@Override
				public void Update(float deltatime)
				{
					final float landingSpeed = 0.15f;
					final float landingDist = 0.5f;

					if (finished)
					{
						return;
					}
					else if (!started)
					{
						landingHeight = GetDist();
						OffsetDist(landingDist);
						started = true;
					}

					OffsetDist(-landingSpeed * deltatime);
					if (GetDist() <= landingHeight)
					{
						SetDist(landingHeight);
						finished = true;

						RenderComposition compModule = (RenderComposition)renderData;
						compModule.HidePart("flameA", true);
						compModule.HidePart("flameB", true);
						compModule.HidePart("flameC", true);
					}

					super.Update(deltatime);
				}

				@Override
				public String GetInteractionType()
				{
					return "LandingModule";
				}

				@Override
				public boolean IsCollider()
				{
					return false;
				}
			};
		}

		float angleRad = angleDeg * MathUtils.degRad;
		moduleEntity.EnterPlanet(this, angleRad);
		moduleEntity.OffsetDist(-scale * 0.06f);
	}

	public void AddPlanetSwitch(float angleDeg, final Planet otherPlanet)
	{
		AddPlanetSwitch(angleDeg, otherPlanet, TriggerType.PLANET_ZONE);
	}
	
	public void AddPlanetSwitch(float angleDeg,
		final Planet otherPlanet, TriggerType triggerType)
	{
		Entity pswitch = new PlanetSwitch(otherPlanet, triggerType);
		
		float angleRad = angleDeg * MathUtils.degRad;
		pswitch.EnterPlanet(this, angleRad);
	}

	static int triggerCount = 0;
	public String AddTrigger(float angleDeg, TriggerType triggerType)
	{
		RenderData empty = RenderData.CreateEmpty(0.f);
		
		Entity trigger = new Entity(empty)
		{
			String name = "Trigger" + triggerCount++;
			
			@Override
			public String GetInteractionType()
			{
				return name;
			}
		};

		float angleRad = angleDeg * MathUtils.degRad;
		trigger.EnterPlanet(this, angleRad);
		
		trigger.EnableAI(triggerType);

		trigger.haloEnabled = false;
		
		return trigger.GetInteractionType();
	}
	
	public void Release()
	{
		surface.Release();
		surface = null;

		background.Release();
		background = null;
		
		updateLock = true;
		for (Entity entity : entities)
		{
			if (!requestRemoval.contains(entity))
			{
				entity.Release();
			}
		}
		updateLock = false;
		
		entities.clear();
	}
	
	public void rotate(float angleSpeed)
	{
		this.angleSpeed = angleSpeed;
	}
	
	public void SetBackgroundAlpha(float alpha)
	{
		background.SetAlpha(alpha);
	}
	
	public float GetBackgroundAlpha()
	{
		return background.GetAlpha();
	}
	
	public void Update(float deltatime)
	{
		updateLock = true;
		for (Entity entity : entities)
		{
			if (!requestRemoval.contains(entity))
			{
				entity.Update(deltatime);
			}
		}
		updateLock = false;
		
		for (Entity entity : requestAddition)
		{
			entities.add(entity);
		}
		requestAddition.clear();
		
		for (Entity entity : requestRemoval)
		{
			entities.remove(entity);
		}
		requestRemoval.clear();
		
		angle += angleSpeed * deltatime;
		if (angle > Math.PI * 2.f)
			angle -= Math.PI * 2.f;
		else if (angle < 0.f)
			angle += Math.PI * 2.f;
			
		backgroundAngle +=
			backgroundSpeed * deltatime;
		if (backgroundAngle > Math.PI * 2.f)
			backgroundAngle -= Math.PI * 2.f;
		else if (backgroundAngle < 0.f)
			backgroundAngle += Math.PI * 2.f;
	}
	
	public void Draw(Matrix4 mat, SpriteBatch batch)
	{		
		mat.translate(center.x, center.y, 0);
		mat.rotateRad(0, 0, 1, -angle);

		mat.rotateRad(0, 0, 1, -backgroundAngle);
		batch.setTransformMatrix(mat);
		
		batch.begin();
		background.Draw(batch);
		batch.end();

		mat.rotateRad(0, 0, 1, backgroundAngle);
		batch.setTransformMatrix(mat);
		
		batch.begin();
		surface.Draw(batch);
		batch.end();

		for(Integer renderOrder : renderEntities.keySet())
		{
			for (Entity entity : renderEntities.get(renderOrder))
			{
				if (entity.hide)
					continue;

				entity.Draw(mat, batch);
			}
		}

		// Second pass for halo (should be on top even for holded objects)
		for(Integer renderOrder : renderEntities.keySet())
		{
			for (Entity entity : renderEntities.get(renderOrder))
			{
				if (entity.hide)
					continue;

				entity.DrawHalo(mat, batch);
			}
		}
		
		mat.rotateRad(0, 0, 1, angle);
		mat.translate(-center.x, -center.y, 0);
	}
	
	public void DebugDraw(Matrix4 mat, ShapeRenderer shapes)
	{		
		mat.translate(center.x, center.y, 0);
		mat.rotateRad(0, 0, 1, -angle);
		shapes.setTransformMatrix(mat);
		
		shapes.begin(ShapeRenderer.ShapeType.Line);
		shapes.setColor(0, 0, 0, 1);
		shapes.circle(0.f, 0.f, radius, 50);
		shapes.end();
		
		for (Entity entity : entities)
		{
			entity.DebugDraw(mat, shapes);
		}

		mat.rotateRad(0, 0, 1, angle);
		mat.translate(-center.x, -center.y, 0);
	}
	
	public Entity Intersect(Vector2 pos)
	{
		return Intersect(pos.x, pos.y);
	}

	public Entity Intersect(float x, float y)
	{
		float localX = x - center.x;
		float localY = y - center.y;
		
		float cosa = (float)Math.cos(angle);
		float sina = (float)Math.sin(angle);

		float rotX = localX * cosa - localY * sina;
		float rotY = localX * sina + localY * cosa;
		
		for (Entity entity : entities)
		{
			if (entity.hide)
				continue;
			
			if (entity.Intersect(rotX, rotY))
			{
				return entity;
			}
		}
		
		return null;
	}
	
	public float ConvertToAngle(Vector2 target)
	{
		return ConvertToAngle(target.x, target.y);
	}
	
	public float ConvertToAngle(float x, float y)
	{
		float localX = x - center.x;
		float localY = y - center.y;

		float length = Vector2.len(localX, localY);
		if (length < 0.01f)
		{
			// too much close to center
			return 0.f;
		}
		
		localX /= length;
		localY /= length;
		
		float localAngle = (float)Math.atan2(localX, localY);
		if (localAngle < 0.f)
			localAngle += Math.PI * 2.f;
		
		return localAngle - angle;
	}
	
	public void ConvertToWorldPos(
		Entity entity, Vector2 pos)
	{
		float dist = entity.GetDist()
			+ 0.5f * entity.GetHeight();
			
		ConvertToWorldPos(
			entity.angle, dist, pos);
	}
	
	public void ConvertToLocalPos(
		Entity entity, Vector2 pos)
	{
		float dist = entity.GetDist()
			+ 0.5f * entity.GetHeight();
		
		ConvertToLocalPos(entity.angle, dist, pos);
	}
	
	public void ConvertToWorldPos(
		float localAngle,
		float dist, Vector2 pos)
	{
		ConvertToLocalPos(localAngle, dist, pos);
		
		pos.x += center.x;
		pos.y += center.y;
	}
	
	public void ConvertToLocalPos(
		float localAngle, float dist,
		Vector2 pos)
	{
		float angle = this.angle + localAngle;
		
		float cosa = (float)Math.cos(angle);
		float sina = (float)Math.sin(angle);

		pos.x = dist * sina;
		pos.y = dist * cosa;
	}
	
	public float DiffAngle(float startAngle, float targetAngle)
	{
		float diffAngle = targetAngle - startAngle;
		if (diffAngle > Math.PI)
			diffAngle -= Math.PI * 2.f;
		else if (diffAngle < -Math.PI)
			diffAngle += Math.PI * 2.f;
			
		return diffAngle;
	}
	
	public float DiffAngle(float angle)
	{
		// relative to planet focus
		return DiffAngle(-this.angle,
			angle - (float)(Math.PI * 2.f));
	}
	
	public float DiffAngle(Entity entity)
	{
		// relative to planet focus
		return DiffAngle(entity.angle);
	}
	
	public float Distance(Entity entity,
		Vector2 target)
	{
		return Distance(entity,
			target.x, target.y);
	}
	
	public float Distance(Entity entity,
		float x, float y)
	{		
		float targetAngle = ConvertToAngle(x, y);
		return Distance(entity, targetAngle);
	}
	
	public float Distance(float startAngle, float targetAngle)
	{
		return radius * DiffAngle(
			startAngle, targetAngle);		
	}
	
	public float Distance(float startAngle, Entity toEntity)
	{
		return Distance(startAngle, toEntity.angle);		
	}
	
	public float Distance(Entity fromEntity, float targetAngle)
	{
		return Distance(fromEntity.angle, targetAngle);		
	}
	
	public float Distance(Entity fromEntity, Entity toEntity)
	{
		return Distance(fromEntity, toEntity.angle);
	}

	public interface EntityFunction
	{
		public void apply(Entity entity);
	}
	public void ApplyToEntities(EntityFunction function)
	{
		for (Entity entity : entities)
		{
			function.apply(entity);
		}
	}
	
	static int checkSortIdx = 0;
	public void CheckSort()
	{
		if (sorted.IsEmpty())
		{
			if (entities.size() > 0)
			{
				Exceptions.PushSortWrongCount(
					checkSortIdx++, 0, entities.size());
			}
			
			return;
		}
		
		int count = 1;
		
		// search for first (smallest) entity 
		Entity first = sorted.GetHead();
		Entity prev = first.GetPrevEntity();
		while (prev != null && prev != sorted.GetHead())
		{
			if (prev.angle <= first.angle)
			{
				first = prev;
			}

			prev = prev.GetPrevEntity();
		}
		
		// check sort for smallest to biggest
		Entity ent = first;
		Entity next = ent.GetNextEntity();
		while (next != null && next != first)
		{
			count++;
			
			if (ent.angle > next.angle)
			{				
				Exceptions.PushSortWrong(
					checkSortIdx,
					ent.GetInteractionType(), ent.angle,
					next.GetInteractionType() , next.angle);
			}
			
			Entity temp = next;
			next = next.GetNextEntity();
			ent = temp;
		}
		
		if (count != entities.size())
		{
			Exceptions.PushSortWrongCount(
				checkSortIdx, count, entities.size());
		}
		
		checkSortIdx++;
	}
}
