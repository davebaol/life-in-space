package com.chernobyl.lifeinspace;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.chernobyl.common.render.Billboard;
import com.chernobyl.common.render.RenderData;
import com.chernobyl.common.utils.JBTHelpers;
import com.chernobyl.common.utils.JBTHelpers.AgentTask;
import com.chernobyl.common.utils.JBTHelpers.TriggerType;
import com.chernobyl.common.utils.IntrusiveLinkedObject;

public class Entity extends IntrusiveLinkedObject<Entity>
{
	static final float gravity = -2.f;
	
	Planet curPlanet;
	
	float angle;
	float angleSpeed;

	float jumpHeight;
	float jumpSpeed;
	
	private float dist;
	public float GetDist()
	{
		return dist + jumpHeight;
	}
	public void SetDist(float dist)
	{
		this.dist = dist;
	}
	public void OffsetDist(float offset)
	{
		this.dist += offset;
	}
	
	int renderOrder;
	
	float scale;

	float collWidthRatio;
	boolean collPlayerOnly;

	boolean isPlayer;

	Character holder;
	
	boolean hide;
	RenderData renderData;
	
	AgentTask agentTask;
	
	static final float haloSizeRatio = 1.5f;
	static final float haloBlinkTime = 0.5f;
	static final float haloStartDelay = 0.5f;
	static final float haloRepeatDelay = 2.f - 0.5f * haloBlinkTime;
	
	static RenderData halo;
	static int haloCount;
	
	static void InitHalo()
	{
		if (++haloCount == 1)
		{
			halo = new Billboard("halo.png",
				haloSizeRatio, haloSizeRatio);
		}
	}
	
	static void ReleaseHalo()
	{
		if (--haloCount == 0)
		{
			halo.Release();
			halo = null;
		}
	}

	boolean haloEnabled;
	float haloBlinkTimeLeft;
	float haloRepeatDelayLeft;

	public Entity(RenderData renderData)
	{
		this.renderData = renderData;

		dist = 1.f;
		angle = 0.f;
		
		scale = 1.f;

		collWidthRatio = 1.f;

		collPlayerOnly = false;
		isPlayer = false;
		
		jumpHeight = 0.f;
		jumpSpeed = 0.f;

		haloEnabled = true;

		InitHalo();
	}

	public String GetInteractionType()
	{
		return "Object";
	}

	public void Update(float deltatime)
	{
		renderData.Update(deltatime);
		
		if (!IsHolded() && angleSpeed != 0.f)
		{
			if (CheckCollision(angleSpeed > 0.f ? 1 : -1))
			{
				angleSpeed = 0.f;
			}
			else
			{
				angle += angleSpeed * deltatime;
				RefreshSort(angleSpeed > 0.f);				
			}
		}
		
		if (!IsHolded() &&
			(jumpHeight > 0.f || jumpSpeed > 0.f))
		{
			jumpSpeed += gravity * deltatime;
			jumpHeight += jumpSpeed * deltatime;
			
			if (jumpHeight <= 0.f)
			{
				jumpHeight = 0.f;
				jumpSpeed = 0.f;
			}
		}
		
		if (haloBlinkTimeLeft > 0.f)
		{
			haloBlinkTimeLeft -= deltatime;
		}
		else if (haloRepeatDelayLeft > 0.f)
		{
			haloRepeatDelayLeft -= deltatime;
			if (haloRepeatDelayLeft <= 0.f)
			{
				haloRepeatDelayLeft += haloRepeatDelay;
				haloBlinkTimeLeft += haloBlinkTime;
			}
		}
		
		UpdateAI();
	}
	
	public void RefreshSort(boolean forward)
	{
		if (forward)
		{
			if (angle > Math.PI * 2.f)
			{
				angle -= Math.PI * 2.f;
				RefreshSort(false);
			}
			
			Entity first = GetNextEntity();
			Entity last = GetPrevEntity();
			Entity next = first;
			Entity prev = last;
			while (next != null
				&& next.angle < angle
				&& (angle < prev.angle
					|| next.angle >= prev.angle))
			{
				GetLink().InsertAfter(next.GetLink());
				
				if (next == last)
					break;
				
				next = GetNextEntity();
				prev = GetPrevEntity();
			}
		}
		else
		{
			if (angle < 0.f)
			{
				angle += Math.PI * 2.f;
				RefreshSort(true);
			}

			Entity first = GetPrevEntity();
			Entity last = GetNextEntity();
			Entity prev = first;
			Entity next = last;
			while (prev != null
				&& prev.angle > angle
				&& (angle > next.angle
					|| prev.angle <= next.angle))
			{
				GetLink().InsertBefore(prev.GetLink());

				if (prev == last)
					break;
				
				prev = GetPrevEntity();
				next = GetNextEntity();
			}
		}
	}
	
	private boolean CheckCollision(int dir)
	{
		if (IsHolded())
			return false;
		
		Entity nextEntity = dir > 0 ?
			GetNextEntity() : GetPrevEntity();
			
		Entity first = nextEntity;
		while (nextEntity != null &&
			(nextEntity.hide ||
			 nextEntity.IsHolded() ||
			!nextEntity.IsCollider()))
		{
			nextEntity = dir > 0 ?
				nextEntity.GetNextEntity()
				: nextEntity.GetPrevEntity();
				
			if (nextEntity == first)
				return false;
		}
		
		if (nextEntity == null)
			return false;

		if (nextEntity.collPlayerOnly && !isPlayer)
			return false;
		
		float dist = curPlanet.Distance(this, nextEntity)*(float)dir;
		if (dist < 0.f)
			return false;
		
		return dist < 0.5f * nextEntity.GetCollWidth();
	}
	
	public Entity GetPrevEntity()
	{
		Entity prev = GetLink().Prev();
		if (prev == this)
			return null;
		else
			return prev;
	}
	
	public Entity GetNextEntity()
	{
		Entity next = GetLink().Next();
		if (next == this)
			return null;
		else
			return next;
	}
	
	public Planet GetPlanet()
	{
		return curPlanet;
	}
	
	public void EnterPlanet(Planet planet, float angle)
	{
		if (curPlanet != null)
		{
			curPlanet.RemoveEntity(this);
		}
		
		curPlanet = planet;
		planet.AddEntity(this, angle);
	}
	
	public void Use(Character character) {}

	public boolean Intersect(Vector2 pos)
	{
		return Intersect(pos.x, pos.y);
	}

	public boolean Intersect(float x, float y)
	{
		float cosa;
		float sina;

		float localX;
		float localY;

		if (IsHolded())
		{
			cosa = (float)Math.cos(holder.angle);
			sina = (float)Math.sin(holder.angle);

			localX = x * cosa - y * sina;
			localY = x * sina + y * cosa;
			localY -= holder.GetDist();

			localX /= holder.scale;
			localY /= holder.scale;

			Vector2 holdingPos = holder.manekin.GetHoldingPos();
			localX -= holdingPos.x;
			localY -= holdingPos.y;

			float relScale = holder.scale / scale;

			localX *= relScale;
			localY *= relScale;
		}
		else
		{
			cosa = (float)Math.cos(angle);
			sina = (float)Math.sin(angle);

			localX = x * cosa - y * sina;
			localY = x * sina + y * cosa;
			localY -= dist + jumpHeight;

			localX /= scale;
			localY /= scale;
		}
		
		return renderData.Intersect(localX, localY);
	}

	public float GetWidth()
	{
		return scale *
			renderData.GetBounds().getWidth();
	}

	public float GetCollWidth()
	{
		return scale * collWidthRatio *
				renderData.GetBounds().getWidth();
	}

	public boolean IsCollPlayerOnly()
	{
		return collPlayerOnly;
	}

	public float GetHeight()
	{
		return scale *
			renderData.GetBounds().getHeight();
	}
	
	public float GetAngle()
	{
		return angle;
	}
	
	public void SetScale(float scale)
	{
		this.scale = scale;
	}
	
	public void InstantJump(float height)
	{
		jumpHeight = height;
	}
	
	public void Jump(float speed)
	{
		jumpSpeed += speed;
	}
	
	public void InstantMove(float diff)
	{
		Move(0.f);
		
		if (!CheckCollision(diff > 0.f ? 1 : -1))
		{
			angle += diff / dist;
			RefreshSort(diff > 0.f);
		}
	}
	
	public void Move(float speed)
	{
		angleSpeed = speed / dist;
	}
	
	public boolean IsMoving()
	{
		return angleSpeed != 0.f;
	}
	
	public void EnableAI(TriggerType triggerType)
	{
		if (agentTask == null)
		{
			agentTask = JBTHelpers.GetAgentTask(triggerType, this);
		}
	}
	
	private void UpdateAI()
	{
		if (agentTask != null)
		{
			if (!agentTask.run())
			{
				agentTask = null;
			}
		}
	}
	
	public void DisableAI()
	{
		if (agentTask != null)
		{
			agentTask.forceFinish();
			agentTask = null;
		}		
	}
	
	public boolean HasAI()
	{
		return agentTask != null;
	}

	private static void InitDrawMatrix(Matrix4 mat, SpriteBatch batch,
		float angle, float dist, float scale)
	{
		float cosa = (float)Math.cos(angle);
		float sina = (float)Math.sin(angle);
		
		float localX = sina * dist;
		float localY = cosa * dist;
			
		// prepare referential for render data
		
		mat.translate(localX, localY, 0);
		mat.rotateRad(0, 0, 1, -angle);
		mat.scale(scale, scale, scale);
		
		batch.setTransformMatrix(mat);
		
		float invScale = 1.f / scale;
		mat.scale(invScale, invScale, invScale);
		mat.rotateRad(0, 0, 1, angle);
		mat.translate(-localX, -localY, 0);
	}

	private static void InitDebugDrawMatrix(Matrix4 mat, ShapeRenderer shapes,
		float angle, float dist, float scale)
	{
		float cosa = (float)Math.cos(angle);
		float sina = (float)Math.sin(angle);

		float localX = sina * dist;
		float localY = cosa * dist;
		
		// prepare referential for render data
		mat.translate(localX, localY, 0);
		mat.rotateRad(0, 0, 1, -angle);
		mat.scale(scale, scale, scale);

		shapes.setTransformMatrix(mat);

		float invScale = 1.f / scale;
		mat.scale(invScale, invScale, invScale);
		mat.rotateRad(0, 0, 1, angle);
		mat.translate(-localX, -localY, 0);
	}

	public void Draw(Matrix4 mat, SpriteBatch batch)
	{
		if (IsHolded())
		{
			return;
		}

		InitDrawMatrix(mat, batch,
				angle, dist + jumpHeight, scale);

		batch.begin();

		renderData.Draw(batch);
		
		batch.end();
	}

	public void DrawHalo(Matrix4 mat, SpriteBatch batch)
	{
		if (IsHolded())
		{
			InitDrawMatrix(mat, batch,
				holder.angle, holder.GetDist(), holder.scale);

			float relScale = holder.scale / scale;
			float invScale = 1.f / relScale;

			Vector2 holdingPos = holder.manekin.GetHoldingPos();

			Matrix4 drawMat = batch.getTransformMatrix();
			drawMat.translate(holdingPos.x, holdingPos.y, 0.f);
			drawMat.scale(invScale, invScale, invScale);
			batch.setTransformMatrix(drawMat);
		}
		else
		{
			InitDrawMatrix(mat, batch,
					angle, dist + jumpHeight, scale);
		}

		batch.begin();

		if (haloBlinkTimeLeft > 0.f &&
				haloBlinkTimeLeft < haloBlinkTime)
		{
			float progress = haloBlinkTimeLeft / haloBlinkTime;

			((Billboard)halo).SetAlpha(progress);

			float width = GetWidth();
			float height = GetHeight();

			float haloScale = 0.5f * (width + height);

			Matrix4 drawMat = batch.getTransformMatrix();
			drawMat.scale(haloScale, haloScale, haloScale);
			batch.setTransformMatrix(drawMat);

			halo.Draw(batch);
		}

		batch.end();
	}

	public void DebugDraw(Matrix4 mat, ShapeRenderer shapes)
	{
		if (IsHolded())
		{
			return;
		}

		InitDebugDrawMatrix(mat, shapes,
			angle, dist + jumpHeight, scale);

		// draw from data
		shapes.begin(ShapeRenderer.ShapeType.Line);
		renderData.DebugDraw(shapes);
		shapes.end();
	}

	public void Release()
	{
		// TODO: hotfix! investigate crashes due to releasing the same seed twice
		if (curPlanet == null)
		{
			return;
		}

		curPlanet.RemoveEntity(this);
		curPlanet = null;
		
		renderData.Release();
		renderData = null;
		
		ReleaseHalo();
	}
	
	public void Hide(boolean hide)
	{
		this.hide = hide;
	}
	
	public boolean HasPart(String name)
	{
		return renderData.HasPart(name);
	}
	
	public void HidePart(String name, boolean hide)
	{
		renderData.HidePart(name, hide);
	}
	
	public boolean IsPartHidden(String name)
	{
		return renderData.IsPartHidden(name);
	}
	
	public void AddEffect(RenderData.Effect effect)
	{
		renderData.AddEffect(effect);
	}
	
	public void SetHold(Character holder)
	{
		this.holder = holder;
	}
	
	public boolean IsHolded()
	{
		return (holder != null);
	}
	
	public void TriggerHalo()
	{
		TriggerHalo(false);
	}
	
	public void TriggerHalo(boolean repeat)
	{
		haloBlinkTimeLeft = haloStartDelay + haloBlinkTime;
		
		if (repeat)
		{
			haloRepeatDelayLeft = haloRepeatDelay;
		}
	}
	
	public void StopHalo()
	{
		haloBlinkTimeLeft = 0.f;
		haloRepeatDelayLeft = 0.f;
	}
	
	public boolean IsCollider()
	{
		return false;
	}
}

