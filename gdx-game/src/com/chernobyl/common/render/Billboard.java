package com.chernobyl.common.render;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.badlogic.gdx.math.Rectangle;

import com.chernobyl.common.render.RenderData;
import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.RenderHelpers;

public class Billboard
	extends RenderData
{
	String assetName;
	Sprite sprite;
	Rectangle bounds;
	
	Texture defaultTexture = null;
	int texOverrideCount = 0;

	static private class TextureData
	{
		Texture texture;
		int used = 0;
	}	
	static HashMap<String, TextureData> textureCache;
	static public Texture GetTexture(String assetName)
	{
		if (textureCache == null)
		{
			textureCache = new HashMap<String, TextureData>();
		}

		TextureData textureData = textureCache.get(assetName);
		if (textureData == null)
		{
			textureData = new TextureData();
			textureData.texture =
				RenderHelpers.LoadTexture(assetName);
			
			textureCache.put(assetName, textureData);
		}

		textureData.used++;
		return textureData.texture;
	}

	static public void ReleaseTexture(String assetName)
	{
		if (textureCache == null)
			return;

		TextureData textureData = textureCache.get(assetName);
		if (textureData == null)
			return;

		textureData.used--;
		if (textureData.used == 0)
		{
			textureData.texture.dispose();
			textureCache.remove(assetName);
		}
	}

	public static Effect AssetChangerEffect(final String tempName, float start, float duration)
	{		
		return new RenderData.Effect(start, duration)
		{
			Texture tempTexture;

			@Override
			public void Init(RenderData renderData)
			{
				tempTexture = GetTexture(tempName);				
			}

			@Override
			public void Start(RenderData renderData)
			{							
				Billboard billboard = (Billboard)renderData;
				billboard.OverrideTexture(tempTexture);
			}

			@Override
			public void Finish(RenderData renderData)
			{
				Billboard billboard = (Billboard)renderData;
				billboard.SetDefaultTexture();				
			}

			@Override
			public void Release(RenderData renderData)
			{
				ReleaseTexture(tempName);				
			}
		};
	}

	public Billboard(String assetName)
	{
		this(assetName, 1.0f);
	}

	public Billboard(String assetName, float scale)
	{
		this.assetName = assetName;

		sprite = new Sprite(GetTexture(assetName));
		sprite.setCenterX(0.f);
		sprite.setOrigin(
			sprite.getWidth() * 0.5f, 0.f);
		sprite.setScale(scale);

		bounds = new Rectangle();
		bounds.set(sprite.getBoundingRectangle());
	}
	
	public Billboard(String assetName,
		float width, float height)
	{
		this(assetName, width, height, true);
	}
	
	public Billboard(String assetName,
		float width, float height, boolean rooted)
	{
		this.assetName = assetName;

		sprite = new Sprite(GetTexture(assetName));
		
		if (rooted)
		{
			sprite.setSize(width, height);
			sprite.setCenterX(0.f);
			sprite.setOrigin(width * 0.5f, 0.f);
		}
		else
		{
			sprite.setBounds(
				-0.5f * width, -0.5f * height,
				width, height);
			sprite.setOrigin(width * 0.5f, height * 0.5f);
		}

		bounds = new Rectangle();
		bounds.set(sprite.getBoundingRectangle());
	}
	
	public void OverrideTexture(Texture tex)
	{
		if (texOverrideCount <= 0)
		{
			texOverrideCount = 1;
			defaultTexture = sprite.getTexture();
		}
		else
		{
			texOverrideCount++;
		}
		
		sprite.setTexture(tex);
	}
	
	public void SetDefaultTexture()
	{
		if (--texOverrideCount == 0)
		{
			sprite.setTexture(defaultTexture);
			defaultTexture = null;
		}
	}

	public void SetPivot(float x, float y)
	{
		sprite.setOrigin(x, y);
		bounds.set(sprite.getBoundingRectangle());
	}

	public void SetPos(float x, float y)
	{
		sprite.setPosition(x, y);
		bounds.set(sprite.getBoundingRectangle());
	}

	public void SetRot(float angleDeg)
	{
		sprite.setRotation(angleDeg);
		bounds.set(sprite.getBoundingRectangle());
	}

	public void SetPosRot(float x, float y, float angleDeg)
	{
		sprite.setPosition(x, y);
		sprite.setRotation(angleDeg);
		bounds.set(sprite.getBoundingRectangle());
	}

	public void SetScale(float scale)
	{
		sprite.setScale(scale);
		bounds.set(sprite.getBoundingRectangle());
	}

	public void SetAlpha(float alpha)
	{
		sprite.setAlpha(alpha);
	}
	
	public float GetAlpha()
	{
		return sprite.getColor().a;
	}
	
	public void NormalizeSize(float maxWidth, float maxHeight)
	{
		float scaleX = 1.f / maxWidth;
		float scaleY = 1.f / maxHeight;
		
		sprite.setScale(
			sprite.getScaleX() * scaleX,
			sprite.getScaleY() * scaleY);
		
		bounds.set(sprite.getBoundingRectangle());
	}

	@Override
	public void Draw(SpriteBatch batch)
	{
		sprite.draw(batch);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{
		shapes.setColor(0, 1, 0, 1);
		shapes.rect(
			bounds.x, bounds.y,
			bounds.width, bounds.height);
	}

	@Override
	public void Release()
	{
		super.Release();
		ReleaseTexture(assetName);
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}

	public static void CheckCleanMemory(boolean fixAndDelay)
	{
		if (textureCache == null)
			return;
		
		for (Map.Entry<String, TextureData> pair
			: textureCache.entrySet())
		{
			String texName = pair.getKey();
			TextureData tex = pair.getValue();
			
			if (tex.used < 0)
			{
				if (fixAndDelay)
				{
					Exceptions.PushAssetReleasedTwice(
						texName, tex.used);
				}
				else
				{
					Exceptions.ThrowAssetReleasedTwice(
						texName, tex.used);
				}
			}
			// should not be in the cache
			// even after release and used = 0
			else if (fixAndDelay)
			{
				Exceptions.PushAssetNotReleased(
					texName, tex.used);

				if (tex.used > 0 && tex.texture != null)
				{
					tex.texture.dispose();
				}
			}
			else
			{
				Exceptions.ThrowAssetNotReleased(
					texName, tex.used);
			}
		}
		
		textureCache = null;
	}
}
