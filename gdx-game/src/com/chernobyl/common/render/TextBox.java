package com.chernobyl.common.render;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Rectangle;

import com.badlogic.gdx.utils.GdxRuntimeException;

import com.chernobyl.common.utils.TextRenderer;
import com.chernobyl.common.utils.RenderHelpers;

public class TextBox extends RenderData
{
	static final float normalShowCharDelay = 0.05f;
	static final float specialShowCharDelay = 0.3f;
	
	private String text;
	private float width;
	private float offsetX, offsetY;
	private TextRenderer.HAnchor hAnchor;
	
	private Rectangle bounds;
	
	private int showCharLeft;
	private float showNextCharDelay;
	
	public TextBox(String text, float width)
	{
		this.text = text;
		this.width = width;
		this.offsetX = 0.f;
		this.offsetY = 0.f;
		this.hAnchor = TextRenderer.HAnchor.LEFT;
		
		bounds = new Rectangle();
		drawText(null, bounds);
		
		showCharLeft = text.length();
		showNextCharDelay = 0.f;
	}
	
	public void SetOffset(float offsetX,
		float offsetY, boolean alignRight)
	{
		bounds.x -= this.offsetX;
		bounds.y -= this.offsetY;
		
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		
		bounds.x += offsetX;
		bounds.y += offsetY;
		
		if (alignRight)
		{
			if (hAnchor != TextRenderer.HAnchor.RIGHT)
			{
				hAnchor = TextRenderer.HAnchor.RIGHT;
				
				bounds.x -= bounds.width;
			}
		}
		else
		{
			if (hAnchor != TextRenderer.HAnchor.LEFT)
			{
				hAnchor = TextRenderer.HAnchor.LEFT;
				
				bounds.x += bounds.width;
			}
		}
	}
	
	@Override
	public void Update(float deltatime)
	{
		super.Update(deltatime);
		
		updateShowText(deltatime);
	}

	static Quaternion quat = new Quaternion();
	
	@Override
	public void Draw(SpriteBatch batch)
	{
		Matrix4 mat = batch.getTransformMatrix();

		mat.getRotation(quat);
		float angle = quat.getRollRad();

		float cosa = (float)Math.cos(angle);
		float sina = (float)Math.sin(angle);
		
		float rotOffsetX = offsetX * cosa - offsetY * sina;
		float rotOffsetY = offsetX * sina + offsetY * cosa;

		float matTranslX = mat.getValues()[Matrix4.M03];
		float boxMinX = matTranslX + rotOffsetX
			- 0.5f * bounds.width;
		float boxMaxX = matTranslX + rotOffsetX
			+ 0.5f * bounds.width;
		if (boxMinX < 0.f || boxMaxX >
			RenderHelpers.GetScreenRatio())
		{
			if (hAnchor == TextRenderer.HAnchor.LEFT)
			{
				SetOffset(-offsetX, offsetY, true);
			}
			else
			{
				SetOffset(-offsetX, offsetY, false);
			}
			
			// recompute new offset
			rotOffsetX = offsetX * cosa - offsetY * sina;
			rotOffsetY = offsetX * sina + offsetY * cosa;
		}

		float scale = mat.getScaleX();
		float invScale = 1.f / scale;

		mat.rotateRad(0, 0, 1, -angle);
		mat.translate(rotOffsetX, rotOffsetY, 0);
		mat.scale(invScale, invScale, invScale);
		batch.setTransformMatrix(mat);
		
		int numChars = text.length() - showCharLeft;
		if (numChars > 0)
		{
			drawText(batch, null, numChars);
		}
		
		mat.scale(scale, scale, scale);
		mat.translate(-rotOffsetX, -rotOffsetY, 0);
		mat.rotateRad(0, 0, 1, angle);
		batch.setTransformMatrix(mat);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{
		shapes.setColor(0, 0, 0, 1);
		shapes.rect(bounds.x, bounds.y,
			bounds.width, bounds.height);
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}
	
	private boolean drawText(SpriteBatch batch,
		Rectangle outBounds)
	{
		return drawText(batch, outBounds, this.text);
	}

	private boolean drawText(SpriteBatch batch,
		Rectangle outBounds, int numChars)
	{
		return drawText(batch, outBounds,
			this.text.substring(0, numChars));
	}

	private boolean drawText(SpriteBatch batch,
		Rectangle outBounds, String text)
	{
		TextRenderer.get().setWarpWidth(width);
		TextRenderer.get().setScale(3.f);
		
		try
		{
			Rectangle resultBounds =
				TextRenderer.get().drawText(
				batch, text, 0, 0,
				hAnchor, TextRenderer.VAnchor.CENTER);

			if (outBounds != null)
			{
				outBounds.set(resultBounds);
			}

			return true;
		}
		catch (GdxRuntimeException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}

	private void updateShowText(float deltatime)
	{
		if (showCharLeft > 0)
		{
			showNextCharDelay -= deltatime;

			while (showNextCharDelay <= 0.f)
			{
				int pos = text.length() - showCharLeft;

				boolean special = false;
				
				if (text.charAt(pos) == '[')
				{
					showCharLeft--;
					pos++;

					if (text.charAt(pos) != '[')
					{
						special = true;

						while (text.charAt(pos) != ']')
						{
							showCharLeft--;
							if (showCharLeft == 0)
								break;

							pos++;
						}

						while (text.charAt(pos) != '[' &&
							   (showCharLeft == 0 
							   || text.charAt(pos + 1) != '['))
						{
							showCharLeft--;
							if (showCharLeft == 0)
								break;

							pos++;
						}

						while (text.charAt(pos) != ']')
						{
							showCharLeft--;
							if (showCharLeft == 0)
								break;

							pos++;
						}
					}
				}

				if (text.charAt(pos) == '.' ||
					text.charAt(pos) == ',' ||
					text.charAt(pos) == ';' ||
					text.charAt(pos) == '!' ||
					text.charAt(pos) == '?')
				{
					special = true;
				}

				showCharLeft--;
				if (showCharLeft == 0)
					break;

				if (special)
				{
					showNextCharDelay +=
						specialShowCharDelay;
				}
				else
				{
					showNextCharDelay +=
						normalShowCharDelay;
				}
			}
		}
	}
}
