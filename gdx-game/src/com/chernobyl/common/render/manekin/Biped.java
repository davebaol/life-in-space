package com.chernobyl.common.render.manekin;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.chernobyl.common.render.RenderData;

public class Biped extends Manekin
{
	static final float headRadius = 1.f / 15.f;
	
	static final float restStepLeft = 0.75f;
	static final float restStepRight = 0.25f;

	static final float stepStrength = 0.2f;
	
	static final float walkLegOffset = 0.2f;
	static final float restLegOffset = 0.15f;
	
	static final float walkArmAngle = 5.f;
	
	static final float cycleSpeed = 2.f;

	eAppearance appearance;

	Face face;
	
	Sprite body;
	Sprite pelvis;
	Sprite armL, armR;
	Sprite legL, legR;

	Sprite hipL, hipR;
	Sprite shoulderL, shoulderR;

	// temp for color test
	Sprite colorTest;

	Rectangle bounds;

	Vector2 position;
	int dir = 0;

	int moveDir = 0;
	float moveScale = 0.f;

	float step = restStepRight;

	float restFactor = 1.f;
	float restSpeed = 5.f;
	
	AnimAction playAction;
	float playActionCycle = 0.f;
	
	RenderData holdedObject;
	float holdedScale;	
	Vector2 holdingPos;
	
	float cycleSpeedScale = 0.f;

	public Biped(eAppearance appearance)
	{
		this.appearance = appearance;
		Init();
		
		cycleSpeedScale = 1.f;
	}

	private void Init()
	{
		appearance.LoadTextures();

		body = new Sprite(appearance.texBody);
		pelvis = new Sprite(appearance.texPelvis);

		legL = new Sprite(appearance.texLeg);
		legR = new Sprite(appearance.texLeg);

		armL = new Sprite(appearance.texArm);
		armR = new Sprite(appearance.texArm);

		hipL = (appearance.texHipL != null) ? new Sprite(appearance.texHipL) : null;
		hipR = (appearance.texHipR != null) ? new Sprite(appearance.texHipR) : null;

		shoulderL = (appearance.texShoulderL != null) ? new Sprite(appearance.texShoulderL) : null;
		shoulderR = (appearance.texShoulderR != null) ? new Sprite(appearance.texShoulderR) : null;

		face = new Face(appearance, headRadius);

		colorTest = new Sprite(appearance.texColor);
		
		bounds = new Rectangle();

		initBody();
		
		SetDir(1);
	}

	@Override
	public int GetDir()
	{
		return dir;
	}

	@Override
	public void SetDir(int dir)
	{
		if (this.dir == dir)
			return;
		
		face.SetDir(dir);

		armL.setFlip(dir < 0, false);
		armR.setFlip(dir < 0, false);

		legL.setFlip(dir < 0, false);
		legR.setFlip(dir < 0, false);
		
		body.setFlip(dir < 0, false);
		pelvis.setFlip(dir < 0, false);

		colorTest.setFlip(dir < 0, false);

		if (null != hipL)
		{
			hipL.setFlip(dir < 0, false);
		}

		if (null != hipR)
		{
			hipR.setFlip(dir < 0, false);
		}

		if ((this.dir != 0) && (null != shoulderL) && (null != shoulderR))
		{
			shoulderL.setFlip(dir >= 0, false);
			shoulderR.setFlip(dir < 0, false);

			// Invert shoulders on x axis (different size)
			float widthOffset = (shoulderL.getWidth() - shoulderR.getWidth()) * 0.5f;
			float backupX = shoulderL.getX();
			shoulderL.setPosition(shoulderR.getX() - widthOffset, shoulderL.getY());
			shoulderR.setPosition(backupX + widthOffset, shoulderR.getY());
		}

		this.dir = dir;
	}

	@Override
	public void SetMove(float move)
	{
		float absMove = Math.abs(move);
		if (absMove < 0.001f)
		{
			moveDir = 0;
		}
		else
		{
			moveScale = absMove;
			moveDir = (int)(move / absMove);
			SetDir(moveDir);
		}
	}
	
	public void ScaleCycleSpeed(float scale)
	{
		cycleSpeedScale *= scale;
	}
	
	private void initBody()
	{
		legL.setSize(
			0.95f * face.getWidth(),
			1.9f * face.getHeight());
		legL.setOrigin(
			legL.getWidth() * 0.5f,
			legL.getHeight());

		legR.set(legL);

		armL.setSize(
			1.2f * face.getWidth(),
			2.4f * face.getHeight());
		armL.setOrigin(
			armL.getWidth() * 0.5f,
			armL.getHeight() * 0.7f);

		armR.set(armL);
		
		if (shoulderL != null)
		{
			shoulderL.setFlip(true, false);
		}
		
		setBasePose();	
		updateBounds();
	}

	private void setBasePose()
	{
		body.setSize(
			1.0f * face.getWidth(),
			2.0f * face.getHeight());
		body.setOrigin(
			body.getWidth() * 0.5f,
			body.getHeight() * 0.5f);

		colorTest.setSize(
				body.getWidth() * 2.0f,
				body.getHeight() * 2.0f);
		colorTest.setOrigin(
				colorTest.getWidth() * 0.5f,
				colorTest.getHeight() * 0.5f);

		legL.setPosition(
			-legL.getWidth() * 0.5f,
			0.f);
		legR.setPosition(
			-legR.getWidth() * 0.5f,
			0.f);

		body.setPosition(
			-body.getWidth() * 0.5f,
			legL.getHeight() * 0.42f);
			
		initUpperBody();

		colorTest.setPosition(
			colorTest.getX(),
			colorTest.getY() - colorTest.getHeight() * 0.2f);
	}
	
	private void initUpperBody()
	{
		face.SetPos(
			0.f,
			body.getY() + body.getHeight() * 0.70f);

		armL.setPosition(
			-armL.getWidth() * 0.35f,
			face.GetY() - armL.getHeight() * 0.7f);

		armR.setPosition(
			-armR.getWidth() * 0.65f,
			face.GetY() - armR.getHeight() * 0.7f);

		pelvis.setSize(body.getWidth(), body.getHeight());
		pelvis.setOrigin(body.getOriginX(), body.getOriginY());
		pelvis.setPosition(body.getX(), body.getY());

		if (null != hipL)
		{
			hipL.setSize(body.getWidth(), body.getHeight());
			hipL.setPosition(body.getX(), body.getY());
		}

		if (null != hipR)
		{
			hipR.setSize(body.getWidth(), body.getHeight());
			hipR.setPosition(body.getX(), body.getY());
		}

		if (null != shoulderL)
		{
			shoulderL.setSize(armL.getWidth() * 0.9f, armL.getHeight() * 0.9f);
			shoulderL.setPosition(armL.getX() * 0.8f, armL.getY() * 1.3f);
		}

		if (null != shoulderR)
		{
			shoulderR.setSize(armR.getWidth(), armR.getHeight());
			shoulderR.setPosition(armR.getX(), armR.getY());
		}
	}

	private void adjustFrontPose()
	{
		body.setSize(
			body.getWidth() * 1.25f,
			body.getHeight());
		body.setX(-body.getWidth());
		body.setOrigin(
			body.getWidth() * 0.5f,
			body.getHeight() * 0.5f);
		
		legL.translateX(body.getWidth() * 0.25f);
		legR.translateX(body.getWidth() * -0.25f);
		
		armL.translateX(body.getWidth() * 0.5f);
		armR.translateX(body.getWidth() * -0.5f);

		updateBounds();
	}
	
	private void adjustBackPose()
	{
		body.setSize(
			body.getWidth() * 1.25f,
			body.getHeight());
		body.setX(-body.getWidth() * 0.5f);
		body.setOrigin(
			body.getWidth() * 0.5f,
			body.getHeight() * 0.5f);
		
		legL.translateX(body.getWidth() * -0.25f);
		legR.translateX(body.getWidth() * 0.25f);

		armL.translateX(body.getWidth() * -0.5f);
		armR.translateX(body.getWidth() * 0.5f);
		
		updateBounds();
	}
	
	private void cancelFrontPos()
	{
		setBasePose();
		updateBounds();
	}

	private void cancelBackPos()
	{
		setBasePose();
		updateBounds();
	}
	
	private void updateBounds()
	{
		bounds.set(body.getBoundingRectangle());
		bounds.merge(face.getBounds());

		bounds.merge(legL.getBoundingRectangle());
		bounds.merge(legR.getBoundingRectangle());
		
		bounds.merge(armL.getBoundingRectangle());
		bounds.merge(armR.getBoundingRectangle());
	}

	@Override
	public void Update(float deltatime)
	{
		super.Update(deltatime);
		
		if (playAction != null)
		{
			playActionCycle -= deltatime;
			if (playActionCycle <= 0.f)
			{
				StopPlayAction();
			}
			else
			{
				float step = playActionCycle
					/ playAction.time;
				
				switch (playAction)
				{
					default:
					case USE:
						{
							if (step < 0.3f)
							{
								float armAngle = 40.f
									* step / 0.3f;
								armL.setRotation(armAngle);
								armR.setRotation(-armAngle);
							}
							else if (step > 0.7f)
							{
								float armAngle = 40.f
									* (1.f - step) / 0.3f;
								armL.setRotation(armAngle);
								armR.setRotation(-armAngle);
							}
						}
						break;

					case JUMP_START:
					case JUMP_END:
						{
							float bodyBlend = Math.abs(step * 2.f - 1.f);
							bodyBlend = 1.f - 0.25f * (1.f - bodyBlend);
							body.setY(bodyBlend * legL.getHeight() * 0.42f);
							initUpperBody();
						}
						break;
				}
			}
		}
		
		face.Update(deltatime);
		
		if (IsPlayingAction())
			return;
		
		if (moveDir == 0)
		{
			if (step != restStepLeft && step != restStepRight)
			{				
				if (dir == 0)
				{
					step = restStepRight;
				}
				else
				{					
					if (dir > 0)
					{
						float newStep = step +
							deltatime * cycleSpeed * cycleSpeedScale * moveScale;
						newStep -= MathUtils.floor(newStep);
						
						if (step < restStepLeft && newStep > restStepLeft)
							step = restStepLeft;
						else if (step < restStepRight && newStep > restStepRight)
							step = restStepRight;
						else
							step = newStep;
					}
					else if (dir < 0)
					{
						float newStep = step -
							deltatime * cycleSpeed * cycleSpeedScale * moveScale;
						newStep -= MathUtils.floor(newStep);
						
						if (step > restStepLeft && newStep < restStepLeft)
							step = restStepLeft;						
						else if (step > restStepRight && newStep < restStepRight)
							step = restStepRight;
						else
							step = newStep;
					}				
				}
			}

			restFactor += deltatime * restSpeed
					* cycleSpeedScale * moveScale;

			if (restFactor > 1.f)
			{
				restFactor = 1.f;
			}
		}
		else
		{
			step += deltatime * moveDir * cycleSpeed
				* cycleSpeedScale * moveScale;
			step -= MathUtils.floor(step);

			restFactor -= deltatime * restSpeed
					* cycleSpeedScale * moveScale;

			if (restFactor < 0.f)
			{
				restFactor = 0.f;
			}
		}

		float angle = MathUtils.PI2 * step;
		float sina = MathUtils.sin(angle);
		float cosa = MathUtils.cos(angle);

		float legOffset = walkLegOffset * legL.getWidth() * sina;	

		if (moveDir == 0)
		{
			float restRatio = restLegOffset / walkLegOffset;
			if (Math.abs(sina) > restRatio)
			{
				legOffset *= restRatio;
			}
		}

		float legStepL = cosa > 0.f ?
			stepStrength * legL.getWidth() * cosa : 0.f;
		float legStepR = cosa < 0.f ?
			-stepStrength * legR.getWidth() * cosa : 0.f;
		
		legL.setPosition(
			-0.5f * legL.getWidth() + legOffset,
			legStepL);
		legR.setPosition(
			-0.5f * legR.getWidth() - legOffset,
			legStepR);

		float armAngle;
		if (step <= 0.5f)
			armAngle = walkArmAngle * (4.f * step - 1.f);
		else
			armAngle = walkArmAngle * (4.f * (1.f - step) - 1.f);

		float baseAngle = (1.f - restFactor) * dir * walkArmAngle;

		armL.setRotation(baseAngle + armAngle);
		
		if (IsHolding())
			armR.setRotation(dir * 90.f);
		else
			armR.setRotation(baseAngle - armAngle);
	}

	@Override
	public void Draw(SpriteBatch batch)
	{		
		super.Draw(batch);

		final boolean useColorTest = false;
		if (useColorTest)
		{
			colorTest.draw(batch);
			return;
		}
		
		if (playAction == AnimAction.USE)
		{			
			face.Draw(batch);
			armL.draw(batch);
			armR.draw(batch);
			legL.draw(batch);
			legR.draw(batch);
			pelvis.draw(batch);
			body.draw(batch);
			SafeDrawSprite(hipL, batch);
			SafeDrawSprite(hipR, batch);
			SafeDrawSprite(shoulderL, batch);
			SafeDrawSprite(shoulderR, batch);
			return;
		}

		if (dir > 0)
		{
			armL.draw(batch);
		}

		if (dir < 0)
		{
			armR.draw(batch);
			
			if (IsHolding())
			{
				DrawHolding(batch);
			}
		}

		pelvis.draw(batch);

		if (dir >= 0)
		{
			legL.draw(batch);
		}
		else
		{
			legR.draw(batch);
		}
		
		if (dir >= 0)
		{
			legR.draw(batch);
		}
		else
		{
			legL.draw(batch);
		}

		body.draw(batch);

		face.Draw(batch);

		SafeDrawSprite(hipL, batch);
		SafeDrawSprite(hipR, batch);

		if (dir <= 0)
		{
			if (dir < 0)
			{
				SafeDrawSprite(shoulderR, batch);
			}
			
			armL.draw(batch);
		}

		if (dir >= 0)
		{
			SafeDrawSprite(shoulderL, batch);
			
			if (IsHolding())
			{
				DrawHolding(batch);
			}
			
			armR.draw(batch);
			
			SafeDrawSprite(shoulderR, batch);
		}
		else
		{
			SafeDrawSprite(shoulderL, batch);
		}
	}

	private void SafeDrawSprite(Sprite sprite, SpriteBatch batch)
	{
		if (sprite != null)
		{
			sprite.draw(batch);
		}
	}
	
	private void DrawHolding(SpriteBatch batch)
	{
		if (!IsHolding())
			return;

		holdingPos.x = armR.getX()
			+ armR.getOriginX();
		holdingPos.y = armR.getY()
			+ armR.getOriginY();

		float armLength = armR.getHeight() * 0.4f;
		if (dir > 0)
			holdingPos.x += armLength;
		else
			holdingPos.x -= armLength;
			
		Matrix4 mat = batch.getTransformMatrix();
		float scale = mat.getScaleX() / holdedScale;
		float invScale = 1.f / scale;

		Rectangle objBounds =
			holdedObject.GetBounds();

		holdingPos.y -= 0.5f * invScale
			* objBounds.getHeight();
		
		mat.translate(holdingPos.x, holdingPos.y, 0.f);
		mat.scale(invScale, invScale, invScale);
		batch.setTransformMatrix(mat);
		
		holdedObject.Draw(batch);

		mat.scale(scale, scale, scale);
		mat.translate(-holdingPos.x, -holdingPos.y, 0.f);
		batch.setTransformMatrix(mat);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{
		super.DebugDraw(shapes);
		
		face.DebugDraw(shapes);
		
		shapes.setColor(1, 0, 0, 1);
		shapes.rect(
			bounds.x, bounds.y,
			bounds.width, bounds.height);

		if (IsHolding())
		{
			Matrix4 mat = shapes.getTransformMatrix();
			float scale = mat.getScaleX() / holdedScale;
			float invScale = 1.f / scale;

			mat.translate(holdingPos.x, holdingPos.y, 0.f);
			mat.scale(invScale, invScale, invScale);
			shapes.setTransformMatrix(mat);

			holdedObject.DebugDraw(shapes);

			mat.scale(scale, scale, scale);
			mat.translate(-holdingPos.x, -holdingPos.y, 0.f);
			shapes.setTransformMatrix(mat);
		}
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}
	
	@Override
	public boolean HasFace()
	{
		return face != null;
	}
	
	@Override
	public Face GetFace()
	{
		return face;
	}

	@Override
	public void PlayAction(AnimAction action)
	{
		playAction = action;
		playActionCycle = action.time;
		
		switch (action)
		{
			default:
			case USE:
				{
					face.ShowHeadBack(true);
					adjustBackPose();
				}
				break;

			case JUMP_START:
			case JUMP_END:
				{

				}
				break;
		}
	}

	@Override
	public void StopPlayAction()
	{
		switch (playAction)
		{
			default:
			case USE:
				{
					armL.setRotation(0.f);
					armR.setRotation(0.f);
					
					face.ShowHeadBack(false);
					cancelBackPos();
				}
				break;

			case JUMP_START:
			case JUMP_END:
				{
					body.setY(
						legL.getHeight() * 0.42f);
					initUpperBody();
				}
				break;
		}
		
		playAction = null;
		playActionCycle = 0.f;
	}
	
	@Override
	public boolean IsPlayingAction()
	{
		return playAction != null;
	}

	@Override
	public void Release()
	{
		super.Release();

		appearance.ReleaseTextures();
		appearance = null;

		face.Release();
		face = null;
	}

	@Override
	public void HoldObject(RenderData renderData, float scale)
	{
		holdedObject = renderData;
		holdedScale = scale;
		
		if (holdingPos == null)
		{
			holdingPos = new Vector2(0, 0);
		}
		else
		{
			holdingPos.set(0, 0);
		}
	}

	@Override
	public void StopHolding()
	{
		holdedObject = null;
	}

	@Override
	public boolean IsHolding()
	{
		return holdedObject != null;
	}
	
	@Override
	public Vector2 GetHoldingPos()
	{
		return holdingPos;
	}
}
