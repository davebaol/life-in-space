package com.chernobyl.common.render.manekin;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.chernobyl.common.render.RenderData;

public class Vehicle extends Manekin
{
	private static final float wheelRadius = 1.f / 20.f;
	
	eWheeled appearance;
	
	Sprite body;
	Sprite[] wheel = new Sprite[2];
	
	Rectangle bounds;
	
	int dir = 0;
	
	float move = 0.f;
	
	public Vehicle(eWheeled appearance)
	{
		this.appearance = appearance;
		Init();
	}

	private void Init()
	{
		appearance.LoadTextures();

		body = new Sprite(appearance.texBody);
		wheel[0] = new Sprite(appearance.texWheel);
		wheel[1] = new Sprite(wheel[0]);
		
		bounds = new Rectangle();
		
		setPose();
		SetDir(1);
	}
	
	private void setPose()
	{
		float wheelSize = wheelRadius * 2.f;
		
		wheel[0].setSize(wheelSize, wheelSize);
		wheel[1].setSize(wheelSize, wheelSize);
		
		wheel[0].setOrigin(wheelRadius, 0);
		wheel[1].setOrigin(wheelRadius, 0);
		
		body.setSize(
			wheelSize * 5.f,
			wheelSize * 3.f);
		
		body.setPosition(
			-0.5f * body.getWidth(),
			wheelRadius);
			
		wheel[0].setPosition(
			-0.5f * wheel[0].getWidth(), 0);
		wheel[1].setPosition(
			-0.5f * wheel[1].getWidth(), 0);
		
		wheel[0].translateX(body.getX()
			+ wheel[0].getWidth());
			
		wheel[1].translateX(body.getX()
			+ body.getWidth()
			- 0.5f * wheel[0].getWidth());
		
		bounds.set(body.getBoundingRectangle());
		bounds.merge(wheel[0].getBoundingRectangle());
		bounds.merge(wheel[1].getBoundingRectangle());
	}
	
	@Override
	public void Update(float deltatime)
	{
		super.Update(deltatime);
		
		if (Math.abs(move) > 0.01f)
		{
			float dist = deltatime * move;
			
			float deltaAngle = -2.f * 180.f * dist
				/ (wheel[0].getWidth() * (float)Math.PI);
			
			wheel[0].rotate(deltaAngle);
			wheel[1].rotate(deltaAngle);
		}
	}
	
	@Override
	public void Draw(SpriteBatch batch)
	{
		super.Draw(batch);
		
		body.draw(batch);
		wheel[0].draw(batch);
		wheel[1].draw(batch);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{
		super.DebugDraw(shapes);
		
		shapes.setColor(1, 0, 0, 1);
		shapes.rect(
			bounds.x, bounds.y,
			bounds.width, bounds.height);
			
		double wheelAngle = 
			(double)wheel[0].getRotation()
			* (Math.PI / 180.d);
			
		float wheelCos = (float)Math.cos(wheelAngle);
		wheelCos *= wheel[0].getWidth();
		float wheelSin = (float)Math.sin(wheelAngle);
		wheelSin *= wheel[0].getHeight();
		
		float wheel0X = wheel[0].getX()
			+ 0.5f * wheel[0].getWidth();
		float wheel0Y = wheel[0].getY()
			+ 0.5f * wheel[0].getHeight();
		shapes.line(wheel0X, wheel0Y,
			wheel0X - wheelSin * 0.15f,
			wheel0Y + wheelCos * 0.15f);
			
		float wheel1X = wheel[1].getX()
			+ 0.5f * wheel[1].getWidth();
		float wheel1Y = wheel[1].getY()
			+ 0.5f * wheel[1].getHeight();
		shapes.line(wheel1X, wheel1Y,
			wheel1X - wheelSin * 0.15f,
			wheel1Y + wheelCos * 0.15f);
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}

	@Override
	public void SetMove(float move)
	{
		this.move = move;
	}

	@Override
	public void SetDir(int dir)
	{
		if (dir * this.dir < 0)
		{
			boolean flipped = dir < 0;
			body.setFlip(flipped, false);
			
			wheel[0].setFlip(flipped, false);
			wheel[1].setFlip(flipped, false);
			
			float tempX = wheel[0].getX();
			wheel[0].setX(wheel[1].getX());
			wheel[1].setX(tempX);
			
			float angle = wheel[0].getRotation();
			if (angle < -180.f) angle += 360.f;
			else if (angle > 180.f) angle -= 360.f;
			
			wheel[0].setRotation(-angle);
			wheel[1].setRotation(-angle);
		}
		
		this.dir = dir;
	}

	@Override
	public int GetDir()
	{
		return dir;
	}
	
	@Override
	public boolean HasFace()
	{
		return false;
	}
	
	@Override
	public Face GetFace()
	{
		return null;
	}
	
	@Override
	public void PlayAction(AnimAction action)
	{
	}

	@Override
	public void StopPlayAction()
	{
	}
	
	@Override
	public boolean IsPlayingAction()
	{
		return false;
	}

	@Override
	public void Release()
	{
		super.Release();
		
		appearance.ReleaseTextures();
		appearance = null;
	}

	@Override
	public void HoldObject(RenderData renderData, float scale)
	{
	}

	@Override
	public void StopHolding()
	{
	}

	@Override
	public boolean IsHolding()
	{
		return false;
	}
	
	@Override
	public Vector2 GetHoldingPos()
	{
		return null;
	}
}
