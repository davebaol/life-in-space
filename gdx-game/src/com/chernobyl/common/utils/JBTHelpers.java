package com.chernobyl.common.utils;

import java.util.HashMap;
import java.util.LinkedList;

import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Entity;

import jbt.execution.core.BTExecutorFactory;
import jbt.execution.core.ContextFactory;
import jbt.execution.core.ExecutionTask.Status;
import jbt.execution.core.IBTExecutor;
import jbt.execution.core.IBTLibrary;
import jbt.execution.core.IContext;
import jbt.model.core.ModelTask;

import bts.btlibrary.AILibrary;
import bts.btlibrary.InteractionLibrary;
import com.badlogic.gdx.math.MathUtils;

public class JBTHelpers
{
	private static abstract class btTask
	{
		IBTExecutor btExecutor;
		
		public boolean run()
		{
			btExecutor.tick();			
			return (btExecutor.getStatus() == Status.RUNNING);
		}
		
		public void forceFinish()
		{
			btExecutor.terminate();
		}
	}
		
	
	/////////////////////////// INTERACTION ///////////////////////////

	public static class Interaction extends btTask
	{
		public boolean IsCancellable()
		{
			IContext context = btExecutor.getRootContext();
			return !(Boolean)(context.getVariable("forbidCancel"));
		}
		
		public Entity GetTarget()
		{
			IContext context = btExecutor.getRootContext();
			return (Entity)context.getVariable("target");
		}
	}
	
	private static IBTLibrary interactionLib;
	
	static public boolean HasInteraction(String type)
	{
		if (interactionLib == null)
		{
			interactionLib = new InteractionLibrary();
		}
		
		return interactionLib.getBT(type) != null;
	}
	
	static public Interaction GetInteraction(String type, Entity target, Character actor)
	{
		if (interactionLib == null)
		{
			interactionLib = new InteractionLibrary();
		}

		IContext context = ContextFactory.createContext(interactionLib);
		context.setVariable("target", target);
		context.setVariable("actor", actor);

		context.setVariable("forbidCancel", false);
				
		ModelTask tree = interactionLib.getBT(type);
		if (tree == null)
		{
			// interaction doesn't exist!
			return null;
		}
		
		Interaction interaction = new Interaction();
		interaction.btExecutor = BTExecutorFactory.createBTExecutor(tree, context);
		
		return interaction;
	}
	
	///////////////////////////     AI     ///////////////////////////

	public static class AgentTask extends btTask {}
	
	private static IBTLibrary aiLib;
	
	public enum AiType
	{
		TEST("Test"),
		ANIMAL("Animal"),
		HUMAN("Human");
		
		public String graphName;
		AiType(String name)
		{
			this.graphName = name;
		}
	}
	
	public enum TriggerType
	{
		INTRO_MONOLOGUE("IntroMonologue"),
		PLANET_ZONE_TUTO1("PlanetZoneTuto1"),

		DISCOVER_ROCKS("DiscoverRocks"),
		PLANET_ZONE_TUTO2("PlanetZoneTuto2"),
		PLANET_ZONE_FIRST_BACK("PlanetZoneFirstBack"),
		
		TUTO2_MONOLOGUE("Tuto2Monologue"),
		DISCOVER_VEGETATION_TUTO("DiscoverVegetationTuto"),
		PLANET_ZONE_BIG_PLANET("PlanetZoneBigPlanet"),
		PLANET_ZONE_TUTO1_BACK("PlanetZoneTuto1Back"),
		
		BIG_PLANET_MONOLOGUE("BigPlanetMonologue"),

		BUSH_PLANET_MONOLOGUE1("BushZone1"),
		BUSH_PLANET_MONOLOGUE2("BushZone2"),

		ANIMAL_PLANET_MONOLOGUE1("AnimalsZone1"),
		ANIMAL_PLANET_MONOLOGUE2("AnimalsZone2"),
		
		DISCOVER_BUSHES("DiscoverBushes"),
		BUSHES_SECRET("BushesSecret"),
		
		VEGETATION_CYCLE("VegetationCycle"),
		SEED_CYCLE("SeedCycle"),
		
		DISCOVER_VEGETATION("DiscoverVegetation"),
		AGAIN_VEGETATION("AgainVegetation"),
		PLANET_ZONE("PlanetZone");

		public String graphName;
		TriggerType(String name)
		{
			this.graphName = name;
		}
	}
	
	static public AgentTask GetAgentTask(TriggerType type, Entity agent)
	{
		return GetAgentTask(type.graphName, agent);
	}
	
	static public AgentTask GetAgentTask(AiType type, Character agent)
	{	
		return GetAgentTask(type.graphName, agent);
	}
	
	static private AgentTask GetAgentTask(String graphName, Entity agent)
	{
		if (aiLib == null)
		{
			aiLib = new AILibrary();
		}

		IContext context = ContextFactory.createContext(aiLib);
		context.setVariable("agent", agent);
		
		ModelTask tree = aiLib.getBT(graphName);
		if (tree == null)
		{
			// agent behavior doesn't exist!
			return null;
		}
		
		AgentTask task = new AgentTask();
		task.btExecutor = BTExecutorFactory.createBTExecutor(tree, context);
		
		return task;
	}
	
	
	///////////////////////////    STACKS    ///////////////////////////
	
	private static HashMap<String, LinkedList<Object>> stacks;
	
	public static void PushToStack(String stackName, Object object)
	{
		if (stacks == null)
		{
			stacks = new HashMap<String, LinkedList<Object>>();
		}
		
		LinkedList<Object> objects = stacks.get(stackName);
		if (objects == null)
		{
			objects = new LinkedList<Object>();
			stacks.put(stackName, objects);
		}
		
		objects.add(object);
	}
	
	public static Object PopFromStack(String stackName)
	{
		if (stacks == null)
		{
			return null;
		}

		LinkedList<Object> objects = stacks.get(stackName);
		if (objects == null)
		{
			return null;
		}
		
		if (objects.isEmpty())
			return null;
		
		return objects.removeFirst();
	}
	
	public static boolean IsEmptyStack(String stackName)
	{
		if (stacks == null)
		{
			return true;
		}

		LinkedList<Object> objects = stacks.get(stackName);
		if (objects == null)
		{
			return true;
		}
		
		return objects.isEmpty();
	}
	
	public static void ShuffleStack(String stackName)
	{
		if (stacks == null)
		{
			return;
		}

		LinkedList<Object> objects = stacks.get(stackName);
		if (objects == null)
		{
			return;
		}
		
		int num = objects.size();
		for (int i = 0; i < num; i++)
		{
			int idx = MathUtils.random(num - 1);
			Object obj = objects.remove(idx);
			objects.add(obj);
		}
	}
	
	///////////////////////////    COUNTERS    ///////////////////////////
	
	static class Counter
	{
	    public int value;
	    
	    public Counter() { value = 0; }
	}
	
	private static HashMap<String, Counter> counters;
	
	private static Counter GetCounter(String name)
	{
		if (counters == null)
		{
			counters = new HashMap<String, Counter>();
		}
		
		Counter counter = counters.get(name);
		if (counter == null)
		{
			counter = new Counter();
			counters.put(name, counter);
		}
		
		return counter;
	}
	
	public static void SetCounter(String name, int value)
	{
		Counter counter = GetCounter(name);
		counter.value = value;
	}
	
	public static void IncrCounter(String name)
	{
		Counter counter = GetCounter(name);
		counter.value++;
	}
	
	public static void DecrCounter(String name)
	{
		Counter counter = GetCounter(name);
		counter.value--;
	}
	
	public static int GetCounterValue(String name)
	{
		Counter counter = GetCounter(name);
		return counter.value;
	}
	
	public static boolean CounterIsGreaterThan(String name, int value)
	{
		Counter counter = GetCounter(name);
		return counter.value > value;
	}
	
	public static boolean CounterIsSmallerThan(String name, int value)
	{
		Counter counter = GetCounter(name);
		return counter.value < value;
	}
	

	///////////////////////////    TIMERS    ///////////////////////////
		
	static float curTime;
	
	public static void UpdateTimer(float deltatime)
	{
		curTime += deltatime;
	}
	
	public static float GetCurTime()
	{
		return curTime;
	}
}
