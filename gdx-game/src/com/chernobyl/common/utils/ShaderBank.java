package com.chernobyl.common.utils;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public enum ShaderBank
{
	GREY_SCALE,
	GHOST;
		
	private ShaderProgram shader;
		
	public ShaderProgram get()
	{
		if (shader != null)
			return shader;
				
		switch (this)
		{
			case GREY_SCALE:
			{
				shader = createGreyScaleShader();
			}
			break;
				
			case GHOST:
			{
				shader = createGhostShader();
			}
			break;
		}
			
		if (shader.isCompiled() == false)
		{
			throw new IllegalArgumentException(
				"Error compiling shader: "
				+ shader.getLog());
		}
			
		return shader;
	}
	
	public void dispose()
	{
		if (shader != null)
		{
			shader.dispose();
			shader = null;
		}
	}
	
	public static void disposeAll()
	{
		for (ShaderBank shaderType : values())
		{
			shaderType.dispose();
		}
	}
	
	private static ShaderProgram createGreyScaleShader()
	{
		ShaderProgram shader = new ShaderProgram(
		
			// vertex shader
			"attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
			+ "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
			+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
			+ "uniform mat4 u_projTrans;\n" //
			+ "varying vec4 v_color;\n" //
			+ "varying vec2 v_texCoords;\n" //
			+ "" //
			+ "\n" //
			+ "void main()\n" //
			+ "{\n" //
			+ "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
			+ "   v_color.a = v_color.a * (256.0/255.0);\n" //
			+ "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
			+ "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
			+ "}\n",
			
			// fragment shader
			"#ifdef GL_ES\n" //
			+ "#define LOWP lowp\n" //
			+ "precision mediump float;\n" //
			+ "#else\n" //
			+ "#define LOWP \n" //
			+ "#endif\n" //
			+ "varying LOWP vec4 v_color;\n" //
			+ "varying vec2 v_texCoords;\n" //
			+ "uniform sampler2D u_texture;\n" //
			+ "void main()\n"//
			+ "{\n" //
			+ "  vec4 texColor = texture2D(u_texture, v_texCoords);\n" //
			+ "  float gray = dot(texColor.rgb, vec3(0.299, 0.587, 0.114));\n" //
			+ "  texColor.rgb = vec3(gray);\n" //
			+ "  gl_FragColor = v_color * texColor;\n" //
			+ "}");
			
		return shader;
	}
	
	private static ShaderProgram createGhostShader()
	{
		ShaderProgram shader = new ShaderProgram(

			// vertex shader
			"attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
			+ "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
			+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
			+ "uniform mat4 u_projTrans;\n" //
			+ "varying vec4 v_color;\n" //
			+ "varying vec2 v_texCoords;\n" //
			+ "" //
			+ "\n" //
			+ "void main()\n" //
			+ "{\n" //
			+ "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
			+ "   v_color.a = v_color.a * (200.0/255.0);\n" //
			+ "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
			+ "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
			+ "}\n",

			// fragment shader
			"#ifdef GL_ES\n" //
			+ "#define LOWP lowp\n" //
			+ "precision mediump float;\n" //
			+ "#else\n" //
			+ "#define LOWP \n" //
			+ "#endif\n" //
			+ "varying LOWP vec4 v_color;\n" //
			+ "varying vec2 v_texCoords;\n" //
			+ "uniform sampler2D u_texture;\n" //
			+ "void main()\n"//
			+ "{\n" //
			+ "  vec4 texColor = texture2D(u_texture, v_texCoords);\n" //
			+ "  float gray = dot(texColor.rgb, vec3(0.299, 0.587, 0.114));\n" //
			+ "  vec3 blueColor = vec3(gray) * vec3(0.0, 0.0, 1.0);\n" //
			+ "  texColor.rgb = mix(texColor.rgb, blueColor, 0.5);\n" //
			+ "  gl_FragColor = v_color * texColor;\n" //
			+ "}");
			
		return shader;
	}
}
