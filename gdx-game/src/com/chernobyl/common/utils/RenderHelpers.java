package com.chernobyl.common.utils;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;

import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

import com.chernobyl.common.render.RenderOptions;


public class RenderHelpers
{
	private static final int MAX_FRAME_BUFFER_COUNT = 2;
	private static ArrayList<FrameBuffer> frameBufferStack = new ArrayList<FrameBuffer>(MAX_FRAME_BUFFER_COUNT);
	
	public static FrameBuffer createFrameBuffer(
		float width, float height)
	{
		float realWidth = /*width * */(float)Gdx.graphics.getWidth();
		float realHeight = /*height * */(float)Gdx.graphics.getHeight();
		
		int finalWidth = (int)realWidth;
		if (finalWidth <= 0) finalWidth = 1;

		int finalHeight = (int)realHeight;
		if (finalHeight <= 0) finalHeight = 1;
		
		return new FrameBuffer(
			Pixmap.Format.RGBA8888,
			finalWidth, finalHeight,
			false);
	}
	
	public static boolean BeginFrameBuffer(FrameBuffer frameBuffer)
	{
		if (frameBufferStack.size() == MAX_FRAME_BUFFER_COUNT)
		{
			return false;
		}
		
		frameBufferStack.add(frameBuffer);
		frameBuffer.begin();
		
		//clear the framebuffer with white
		Gdx.graphics.getGL20().glClearColor(1f, 1f, 1f, 1f);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		return true;
	}
	
	public static boolean EndFrameBuffer(FrameBuffer frameBuffer)
	{
		frameBuffer.end();
		frameBufferStack.remove(frameBufferStack.size() - 1);
		
		if (frameBufferStack.size() > 0)
		{
			frameBufferStack.get(frameBufferStack.size() - 1).begin();
		}
		
		return true;
	}
	
	public static float GetScreenRatio()
	{
		return (float)Gdx.graphics.getWidth()
			/ (float)Gdx.graphics.getHeight();
	}
	
	public static float GetDisplaySizeScale()
	{
		final float desiredWidth = 1280.f;
		return (float)Gdx.graphics.getWidth()
			/ desiredWidth;
	}
	
	public static Texture LoadTexture(String assetName)
	{
		FileHandle assetFile = Gdx.files.internal(
			"graphics/" + assetName);
		return LoadTexture(assetFile);
	}
	
	public static Texture LoadTexture(FileHandle assetFile)
	{
		Pixmap pix = new Pixmap(assetFile);

		float scale = RenderHelpers.GetDisplaySizeScale();
		if (scale < 0.75f)
		{
			int realWidth = pix.getWidth();
			int realHeight = pix.getHeight();

			int scaledWidth = (int)((float)realWidth * scale);
			int scaledHeight = (int)((float)realHeight * scale);

			Pixmap scaledPix = new Pixmap(
				realWidth, realHeight,
				pix.getFormat());
				
			Pixmap.setFilter(
				RenderOptions.texResizeFilterMethod);
				
			scaledPix.drawPixmap(scaledPix,
				0, 0, realWidth, realHeight,
				0, 0, scaledWidth, scaledHeight);

			pix.dispose();
			pix = scaledPix;
		}

		Texture texture = new Texture(pix,
			RenderOptions.useMipmap);
		pix.dispose();

		if (RenderOptions.useMipmap)
		{
			texture.setFilter(
				RenderOptions.mipmapMethodMin,
				RenderOptions.mipmapMethodMag);
		}
		
		return texture;
	}
	
	static OrthographicCamera camera;
	static float cameraZoom = 1.f;
	static boolean cameraBlendOut = true;
	static float cameraZoomTargetX;
	static float cameraZoomTargetY;
	static float cameraZoomBlend;
	static float transitionTime;
	
	static OrthographicCamera cameraUI;
	
	public static OrthographicCamera GetCamera()
	{
		if (camera == null)
		{
			camera = CreateCamera();
		}
		
		return camera;
	}
	
	public static OrthographicCamera GetCameraUI()
	{
		if (cameraUI == null)
		{
			cameraUI = CreateCamera();
		}

		return cameraUI;
	}
	
	public static OrthographicCamera CreateCamera()
	{
		float width = GetScreenRatio();
		float height = 1.f;

		OrthographicCamera newCamera
			= new OrthographicCamera(); 
		newCamera.setToOrtho(false, width, height);
		
		return newCamera;
	}
	
	public static void UpdateCamera(float deltatime)
	{
		UpdateCameraZoom(deltatime);
		
		camera.update();
	}
	
	private static void UpdateCameraZoom(float deltatime)
	{
		if (cameraBlendOut)
		{
			if (cameraZoomBlend <= 0.f)
				return;
				
			cameraZoomBlend -=
				deltatime / transitionTime;
			if (cameraZoomBlend < 0.f)
				cameraZoomBlend = 0.f;
		}
		else
		{
			if (cameraZoomBlend >= 1.f)
			{
				// at least update params
				// if they changed
				camera.zoom = cameraZoom;
				camera.position.x = cameraZoomTargetX;
				camera.position.y = cameraZoomTargetY;
				return;
			}
			
			cameraZoomBlend +=
				deltatime / transitionTime;
			if (cameraZoomBlend > 1.f)
				cameraZoomBlend = 1.f;
		}

		camera.zoom = 1.f
			+ (cameraZoom - 1.f)
				* cameraZoomBlend;

		float targetX = 0.5f * camera.viewportWidth;
		float targetY = 0.5f * camera.viewportHeight;

		targetX += (cameraZoomTargetX - targetX)
			* cameraZoomBlend;
		targetY += (cameraZoomTargetY - targetY)
			* cameraZoomBlend;

		camera.position.x = targetX;
		camera.position.y = targetY;
	}
	
	public static void SetCameraZoom(float zoom, float transition)
	{
		float targetX = 0.5f * camera.viewportWidth;
		float targetY = 0.5f * camera.viewportHeight;

		SetCameraZoom(zoom, targetX, targetY, transition);
	}
	
	public static void SetCameraZoom(float zoom)
	{
		float targetX = 0.5f * camera.viewportWidth;
		float targetY = 0.5f * camera.viewportHeight;
		
		SetCameraZoom(zoom, targetX, targetY);
	}
	
	public static void SetCameraZoom(float zoom,
		float targetX, float targetY, float transition)
	{
		transitionTime = transition;
	

		if (zoom == 1.f)
		{
			cameraBlendOut = true;
		}
		else
		{
			cameraBlendOut = false;
			
			cameraZoom = 1.f / zoom;
			cameraZoomTargetX = targetX;
			cameraZoomTargetY = targetY;
		}
	}
	
	public static void SetCameraZoom(float zoom,
		float targetX, float targetY)
	{
		camera.zoom = 1.f / zoom;
		camera.position.x = targetX;
		camera.position.y = targetY;
		
		if (zoom == 1.f)
		{
			cameraZoomBlend = 0.f;
			cameraBlendOut = true;
		}
		else
		{
			cameraZoomBlend = 1.f;
			cameraBlendOut = false;
			
			cameraZoom = camera.zoom;
			cameraZoomTargetX = targetX;
			cameraZoomTargetY = targetY;
		}
	}
	
	public static void ForceCameraZoomBlending(float blend)
	{
		cameraZoomBlend = blend;
	}
}
