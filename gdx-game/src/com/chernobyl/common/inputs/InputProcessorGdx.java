package com.chernobyl.common.inputs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;

public class InputProcessorGdx extends InputProcessor
{
	InputProcessorGdx()
	{
		Gdx.input.setInputProcessor(new InputAdapter()
		{
			int curPointer = -1;

			@Override
			public boolean touchDown(int x, int y, int pointer, int button)
			{
				curPointer = pointer;
				
				NotifyListenersInput(TouchState.PRESS, x, y);
				
				return true;
			}

			@Override
			public boolean touchDragged(int x, int y, int pointer)
			{
				if (pointer == curPointer)
				{
					NotifyListenersInput(TouchState.REPEAT, x, y);
				}

				return true;
			}

			@Override
			public boolean touchUp(int x, int y, int pointer, int button) 
			{
				if (pointer == curPointer)
				{
					NotifyListenersInput(TouchState.RELEASE, x, y);
				}

				return true;
			}
			
			@Override
			public boolean keyDown(int keycode)
			{
				NotifyListenersKeyDown(keycode);
				return true;
			}
		});
	}
	
	@Override
	public boolean Update(float deltaTime)
	{
		NotifyListenersTick(deltaTime);
		return true;
	}
}
