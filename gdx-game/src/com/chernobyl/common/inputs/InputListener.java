package com.chernobyl.common.inputs;

public interface InputListener
{
	abstract public void OnTick(float deltaTime);
	
	abstract public void OnInput(TouchState state, int x, int y);
	
	abstract public void OnKeyDown(int keycode);
}
