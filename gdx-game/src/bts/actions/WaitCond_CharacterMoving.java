// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/10/2016 21:34:44
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action WaitCond_CharacterMoving. */
public class WaitCond_CharacterMoving extends
		jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "character" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object character;
	/**
	 * Location, in the context, of the parameter "character" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String characterLoc;

	/**
	 * Constructor. Constructs an instance of WaitCond_CharacterMoving.
	 * 
	 * @param character
	 *            value of the parameter "character", or null in case it should
	 *            be read from the context. If null, <code>characterLoc</code>
	 *            cannot be null.
	 * @param characterLoc
	 *            in case <code>character</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public WaitCond_CharacterMoving(jbt.model.core.ModelTask guard,
			java.lang.Object character, java.lang.String characterLoc) {
		super(guard);
		this.character = character;
		this.characterLoc = characterLoc;
	}

	/**
	 * Returns a bts.actions.execution.WaitCond_CharacterMoving task that is
	 * able to run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.WaitCond_CharacterMoving(this,
				executor, parent, this.character, this.characterLoc);
	}
}