// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/12/2016 10:16:53
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Hold. */
public class I_Hold extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_Hold. */
	public I_Hold(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_Hold task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Hold(this, executor, parent);
	}
}