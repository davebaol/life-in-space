// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/10/2016 23:43:43
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Counter_IncrAndGet. */
public class Counter_IncrAndGet extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "counterName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String counterName;
	/**
	 * Location, in the context, of the parameter "counterName" in case its
	 * value is not specified at construction time. null otherwise.
	 */
	private java.lang.String counterNameLoc;
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;

	/**
	 * Constructor. Constructs an instance of Counter_IncrAndGet.
	 * 
	 * @param counterName
	 *            value of the parameter "counterName", or null in case it
	 *            should be read from the context. If null,
	 *            <code>counterNameLoc</code> cannot be null.
	 * @param counterNameLoc
	 *            in case <code>counterName</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null, <code>varNameLoc</code> cannot
	 *            be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Counter_IncrAndGet(jbt.model.core.ModelTask guard,
			java.lang.String counterName, java.lang.String counterNameLoc,
			java.lang.String varName, java.lang.String varNameLoc) {
		super(guard);
		this.counterName = counterName;
		this.counterNameLoc = counterNameLoc;
		this.varName = varName;
		this.varNameLoc = varNameLoc;
	}

	/**
	 * Returns a bts.actions.execution.Counter_IncrAndGet task that is able to
	 * run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Counter_IncrAndGet(this, executor,
				parent, this.counterName, this.counterNameLoc, this.varName,
				this.varNameLoc);
	}
}