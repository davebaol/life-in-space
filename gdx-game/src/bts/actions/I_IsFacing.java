// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 01:07:24
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_IsFacing. */
public class I_IsFacing extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_IsFacing. */
	public I_IsFacing(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_IsFacing task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_IsFacing(this, executor, parent);
	}
}