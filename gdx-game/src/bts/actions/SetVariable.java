// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action SetVariable. */
public class SetVariable extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;

	/**
	 * Constructor. Constructs an instance of SetVariable.
	 * 
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null, <code>varNameLoc</code> cannot
	 *            be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null, <code>objectLoc</code> cannot
	 *            be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public SetVariable(jbt.model.core.ModelTask guard,
			java.lang.String varName, java.lang.String varNameLoc,
			java.lang.Object object, java.lang.String objectLoc) {
		super(guard);
		this.varName = varName;
		this.varNameLoc = varNameLoc;
		this.object = object;
		this.objectLoc = objectLoc;
	}

	/**
	 * Returns a bts.actions.execution.SetVariable task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.SetVariable(this, executor, parent,
				this.varName, this.varNameLoc, this.object, this.objectLoc);
	}
}