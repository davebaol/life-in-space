// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/12/2016 09:39:53
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Entity_GetDirectionBetween. */
public class Entity_GetDirectionBetween extends
		jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "object1" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object1;
	/**
	 * Location, in the context, of the parameter "object1" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String object1Loc;
	/**
	 * Value of the parameter "object2" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object2;
	/**
	 * Location, in the context, of the parameter "object2" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String object2Loc;
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;

	/**
	 * Constructor. Constructs an instance of Entity_GetDirectionBetween.
	 * 
	 * @param object1
	 *            value of the parameter "object1", or null in case it should be
	 *            read from the context. If null, <code>object1Loc</code> cannot
	 *            be null.
	 * @param object1Loc
	 *            in case <code>object1</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param object2
	 *            value of the parameter "object2", or null in case it should be
	 *            read from the context. If null, <code>object2Loc</code> cannot
	 *            be null.
	 * @param object2Loc
	 *            in case <code>object2</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null, <code>varNameLoc</code> cannot
	 *            be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_GetDirectionBetween(jbt.model.core.ModelTask guard,
			java.lang.Object object1, java.lang.String object1Loc,
			java.lang.Object object2, java.lang.String object2Loc,
			java.lang.String varName, java.lang.String varNameLoc) {
		super(guard);
		this.object1 = object1;
		this.object1Loc = object1Loc;
		this.object2 = object2;
		this.object2Loc = object2Loc;
		this.varName = varName;
		this.varNameLoc = varNameLoc;
	}

	/**
	 * Returns a bts.actions.execution.Entity_GetDirectionBetween task that is
	 * able to run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Entity_GetDirectionBetween(this,
				executor, parent, this.object1, this.object1Loc, this.object2,
				this.object2Loc, this.varName, this.varNameLoc);
	}
}