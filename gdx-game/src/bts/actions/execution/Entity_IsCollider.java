// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 23:27:00
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.lifeinspace.Entity;

/** ExecutionAction class created from MMPM action Entity_IsCollider. */
public class Entity_IsCollider extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;

	/**
	 * Constructor. Constructs an instance of Entity_IsCollider that is able to
	 * run a bts.actions.Entity_IsCollider.
	 * 
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null,
	 *            <code>objectLoc<code> cannot be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_IsCollider(bts.actions.Entity_IsCollider modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object object,
			java.lang.String objectLoc) {
		super(modelTask, executor, parent);

		this.object = object;
		this.objectLoc = objectLoc;
	}

	/**
	 * Returns the value of the parameter "object", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getObject() {
		if (this.object != null) {
			return this.object;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.objectLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		Entity entity = (Entity)getObject();
		if (entity.IsCollider() && !entity.IsCollPlayerOnly())
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}