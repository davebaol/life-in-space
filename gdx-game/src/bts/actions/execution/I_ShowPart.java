// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/12/2016 10:16:54
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;

/** ExecutionAction class created from MMPM action I_ShowPart. */
public class I_ShowPart extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "part" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String part;
	/**
	 * Location, in the context, of the parameter "part" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String partLoc;

	/**
	 * Constructor. Constructs an instance of I_ShowPart that is able to run a
	 * bts.actions.I_ShowPart.
	 * 
	 * @param part
	 *            value of the parameter "part", or null in case it should be
	 *            read from the context. If null,
	 *            <code>partLoc<code> cannot be null.
	 * @param partLoc
	 *            in case <code>part</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_ShowPart(bts.actions.I_ShowPart modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.String part,
			java.lang.String partLoc) {
		super(modelTask, executor, parent);

		this.part = part;
		this.partLoc = partLoc;
	}

	/**
	 * Returns the value of the parameter "part", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getPart() {
		if (this.part != null) {
			return this.part;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.partLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity target = (Entity)context.getVariable("target");
		target.HidePart(getPart(), false);
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}