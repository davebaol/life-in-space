// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.common.utils.JBTHelpers;

import jbt.execution.core.IContext;

/** ExecutionAction class created from MMPM action Stack_Pop. */
public class Stack_Pop extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "stackFrom" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stackFrom;
	/**
	 * Location, in the context, of the parameter "stackFrom" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String stackFromLoc;
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;

	/**
	 * Constructor. Constructs an instance of Stack_Pop that is able to run a
	 * bts.actions.Stack_Pop.
	 * 
	 * @param stackFrom
	 *            value of the parameter "stackFrom", or null in case it should
	 *            be read from the context. If null,
	 *            <code>stackFromLoc<code> cannot be null.
	 * @param stackFromLoc
	 *            in case <code>stackFrom</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null,
	 *            <code>objectLoc<code> cannot be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Stack_Pop(bts.actions.Stack_Pop modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent,
			java.lang.String stackFrom, java.lang.String stackFromLoc,
			java.lang.Object object, java.lang.String objectLoc) {
		super(modelTask, executor, parent);

		this.stackFrom = stackFrom;
		this.stackFromLoc = stackFromLoc;
		this.object = object;
		this.objectLoc = objectLoc;
	}

	/**
	 * Returns the value of the parameter "stackFrom", or null in case it has
	 * not been specified or it cannot be found in the context.
	 */
	public java.lang.String getStackFrom() {
		if (this.stackFrom != null) {
			return this.stackFrom;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.stackFromLoc);
		}
	}

	/**
	 * Returns the value of the parameter "object", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getObject() {
		if (this.object != null) {
			return this.object;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.objectLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		String stackName = getStackFrom();
		if (stackName == null || stackName.isEmpty())
			return jbt.execution.core.ExecutionTask.Status.FAILURE;

		IContext context = getContext();
		Object object = JBTHelpers.PopFromStack(stackName);
		if (object == null)
		{
			return jbt.execution.core.ExecutionTask.Status.FAILURE;			
		}
		else
		{
			context.setVariable(this.objectLoc, object);
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;	
		}		
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}