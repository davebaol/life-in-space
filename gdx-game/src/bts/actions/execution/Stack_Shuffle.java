// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/12/2016 01:14:19
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.common.utils.JBTHelpers;

/** ExecutionAction class created from MMPM action Stack_Shuffle. */
public class Stack_Shuffle extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "stack" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stack;
	/**
	 * Location, in the context, of the parameter "stack" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String stackLoc;

	/**
	 * Constructor. Constructs an instance of Stack_Shuffle that is able to run
	 * a bts.actions.Stack_Shuffle.
	 * 
	 * @param stack
	 *            value of the parameter "stack", or null in case it should be
	 *            read from the context. If null,
	 *            <code>stackLoc<code> cannot be null.
	 * @param stackLoc
	 *            in case <code>stack</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Stack_Shuffle(bts.actions.Stack_Shuffle modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.String stack,
			java.lang.String stackLoc) {
		super(modelTask, executor, parent);

		this.stack = stack;
		this.stackLoc = stackLoc;
	}

	/**
	 * Returns the value of the parameter "stack", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getStack() {
		if (this.stack != null) {
			return this.stack;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.stackLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		JBTHelpers.ShuffleStack(getStack());
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}