// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action AI_MoveAway. */
public class AI_MoveAway extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "targetAngle" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float targetAngle;
	/**
	 * Location, in the context, of the parameter "targetAngle" in case its
	 * value is not specified at construction time. null otherwise.
	 */
	private java.lang.String targetAngleLoc;

	/**
	 * Constructor. Constructs an instance of AI_MoveAway that is able to run a
	 * bts.actions.AI_MoveAway.
	 * 
	 * @param targetAngle
	 *            value of the parameter "targetAngle", or null in case it
	 *            should be read from the context. If null,
	 *            <code>targetAngleLoc<code> cannot be null.
	 * @param targetAngleLoc
	 *            in case <code>targetAngle</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public AI_MoveAway(bts.actions.AI_MoveAway modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent,
			java.lang.Float targetAngle, java.lang.String targetAngleLoc) {
		super(modelTask, executor, parent);

		this.targetAngle = targetAngle;
		this.targetAngleLoc = targetAngleLoc;
	}

	/**
	 * Returns the value of the parameter "targetAngle", or null in case it has
	 * not been specified or it cannot be found in the context.
	 */
	public java.lang.Float getTargetAngle() {
		if (this.targetAngle != null) {
			return this.targetAngle;
		} else {
			return (java.lang.Float) this.getContext().getVariable(
					this.targetAngleLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
		
		IContext context = getContext();
		Character agent = (Character)context.getVariable("agent");

		Planet planet = agent.GetPlanet();
		
		float diff = planet.Distance(agent, getTargetAngle());
		float oppositeDir = -Math.signum(diff);
		agent.MoveDir(oppositeDir);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}