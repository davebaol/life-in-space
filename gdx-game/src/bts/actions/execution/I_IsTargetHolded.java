// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 04:27:07
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;

/** ExecutionAction class created from MMPM action I_IsTargetHolded. */
public class I_IsTargetHolded extends
		jbt.execution.task.leaf.action.ExecutionAction {

	/**
	 * Constructor. Constructs an instance of I_IsTargetHolded that is able to
	 * run a bts.actions.I_IsTargetHolded.
	 */
	public I_IsTargetHolded(bts.actions.I_IsTargetHolded modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		super(modelTask, executor, parent);

	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity target = (Entity)context.getVariable("target");
		
		if (target.IsHolded())
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}