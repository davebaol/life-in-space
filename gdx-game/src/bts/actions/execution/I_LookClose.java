// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 20:14:07
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action I_LookClose. */
public class I_LookClose extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "distance" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float distance;
	/**
	 * Location, in the context, of the parameter "distance" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String distanceLoc;

	/**
	 * Constructor. Constructs an instance of I_LookClose that is able to run a
	 * bts.actions.I_LookClose.
	 * 
	 * @param distance
	 *            value of the parameter "distance", or null in case it should
	 *            be read from the context. If null,
	 *            <code>distanceLoc<code> cannot be null.
	 * @param distanceLoc
	 *            in case <code>distance</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public I_LookClose(bts.actions.I_LookClose modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Float distance,
			java.lang.String distanceLoc) {
		super(modelTask, executor, parent);

		this.distance = distance;
		this.distanceLoc = distanceLoc;
	}

	/**
	 * Returns the value of the parameter "distance", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Float getDistance() {
		if (this.distance != null) {
			return this.distance;
		} else {
			return (java.lang.Float) this.getContext().getVariable(
					this.distanceLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
		
		IContext context = getContext();
		
		Entity target = (Entity)context.getVariable("target");
		Character actor = (Character)context.getVariable("actor");
		
		Planet planet = actor.GetPlanet();

		float diff = planet.Distance(actor, target);
		if (diff < 0.f)
		{
			if (-diff < distance)
			{
				actor.Move(0.f);
			}
			else
			{
				actor.MoveDiff(diff + distance);
			}
		}
		else
		{
			if (diff < distance)
			{
				actor.Move(0.f);
			}
			else
			{
				actor.MoveDiff(diff - distance);
			}
		}
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity target = (Entity)context.getVariable("target");
		Character actor = (Character)context.getVariable("actor");

		if (actor.IsMoving())
		{
			return jbt.execution.core.ExecutionTask.Status.RUNNING;			
		}
		
		Planet planet = actor.GetPlanet();
		float dist = Math.abs(planet.Distance(actor, target));
		
		if (dist <= getDistance() + 0.01f)
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}