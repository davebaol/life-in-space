// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_MoveDiff. */
public class AI_MoveDiff extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "distance" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float distance;
	/**
	 * Location, in the context, of the parameter "distance" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String distanceLoc;

	/**
	 * Constructor. Constructs an instance of AI_MoveDiff.
	 * 
	 * @param distance
	 *            value of the parameter "distance", or null in case it should
	 *            be read from the context. If null, <code>distanceLoc</code>
	 *            cannot be null.
	 * @param distanceLoc
	 *            in case <code>distance</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public AI_MoveDiff(jbt.model.core.ModelTask guard,
			java.lang.Float distance, java.lang.String distanceLoc) {
		super(guard);
		this.distance = distance;
		this.distanceLoc = distanceLoc;
	}

	/**
	 * Returns a bts.actions.execution.AI_MoveDiff task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_MoveDiff(this, executor, parent,
				this.distance, this.distanceLoc);
	}
}