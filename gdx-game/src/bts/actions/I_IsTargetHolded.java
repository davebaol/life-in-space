// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 04:27:06
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_IsTargetHolded. */
public class I_IsTargetHolded extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_IsTargetHolded. */
	public I_IsTargetHolded(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_IsTargetHolded task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_IsTargetHolded(this, executor,
				parent);
	}
}