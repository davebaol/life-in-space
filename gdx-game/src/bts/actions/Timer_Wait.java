// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/28/2016 10:46:55
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Timer_Wait. */
public class Timer_Wait extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "timeInSec" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float timeInSec;
	/**
	 * Location, in the context, of the parameter "timeInSec" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String timeInSecLoc;

	/**
	 * Constructor. Constructs an instance of Timer_Wait.
	 * 
	 * @param timeInSec
	 *            value of the parameter "timeInSec", or null in case it should
	 *            be read from the context. If null, <code>timeInSecLoc</code>
	 *            cannot be null.
	 * @param timeInSecLoc
	 *            in case <code>timeInSec</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public Timer_Wait(jbt.model.core.ModelTask guard,
			java.lang.Float timeInSec, java.lang.String timeInSecLoc) {
		super(guard);
		this.timeInSec = timeInSec;
		this.timeInSecLoc = timeInSecLoc;
	}

	/**
	 * Returns a bts.actions.execution.Timer_Wait task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Timer_Wait(this, executor, parent,
				this.timeInSec, this.timeInSecLoc);
	}
}