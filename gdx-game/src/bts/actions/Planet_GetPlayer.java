// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Planet_GetPlayer. */
public class Planet_GetPlayer extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "player" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object player;
	/**
	 * Location, in the context, of the parameter "player" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String playerLoc;

	/**
	 * Constructor. Constructs an instance of Planet_GetPlayer.
	 * 
	 * @param player
	 *            value of the parameter "player", or null in case it should be
	 *            read from the context. If null, <code>playerLoc</code> cannot
	 *            be null.
	 * @param playerLoc
	 *            in case <code>player</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Planet_GetPlayer(jbt.model.core.ModelTask guard,
			java.lang.Object player, java.lang.String playerLoc) {
		super(guard);
		this.player = player;
		this.playerLoc = playerLoc;
	}

	/**
	 * Returns a bts.actions.execution.Planet_GetPlayer task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Planet_GetPlayer(this, executor,
				parent, this.player, this.playerLoc);
	}
}