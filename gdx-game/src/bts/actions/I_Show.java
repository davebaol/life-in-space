// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Show. */
public class I_Show extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_Show. */
	public I_Show(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_Show task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Show(this, executor, parent);
	}
}