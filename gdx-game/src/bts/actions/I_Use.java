// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:30
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Use. */
public class I_Use extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_Use. */
	public I_Use(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_Use task that is able to run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Use(this, executor, parent);
	}
}