// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:30
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_LookAt. */
public class I_LookAt extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_LookAt. */
	public I_LookAt(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_LookAt task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_LookAt(this, executor, parent);
	}
}