// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/10/2016 19:51:14
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Counter_Decr. */
public class Counter_Decr extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "counterName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String counterName;
	/**
	 * Location, in the context, of the parameter "counterName" in case its
	 * value is not specified at construction time. null otherwise.
	 */
	private java.lang.String counterNameLoc;

	/**
	 * Constructor. Constructs an instance of Counter_Decr.
	 * 
	 * @param counterName
	 *            value of the parameter "counterName", or null in case it
	 *            should be read from the context. If null,
	 *            <code>counterNameLoc</code> cannot be null.
	 * @param counterNameLoc
	 *            in case <code>counterName</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public Counter_Decr(jbt.model.core.ModelTask guard,
			java.lang.String counterName, java.lang.String counterNameLoc) {
		super(guard);
		this.counterName = counterName;
		this.counterNameLoc = counterNameLoc;
	}

	/**
	 * Returns a bts.actions.execution.Counter_Decr task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Counter_Decr(this, executor, parent,
				this.counterName, this.counterNameLoc);
	}
}