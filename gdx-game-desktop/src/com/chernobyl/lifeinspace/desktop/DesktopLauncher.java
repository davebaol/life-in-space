package com.chernobyl.lifeinspace.desktop;

import com.chernobyl.lifeinspace.GdxGame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {

	static final boolean debugMode = false;
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Life in Space";
		config.width = 1280;
		config.height = 768;
		
		new LwjglApplication(new GdxGame(debugMode), config);
	}
}
